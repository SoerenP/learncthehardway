####Learning C the hard way#####

This repository is consisting of two parts, all with the central theme of learning C the hard way.

The initial part is liblcthw, and the c-skeleton folders. These
stem from the completion of 

	c.learncodethehardway.org/book/

which i can highly recommend. After completion, i added my own implementations
of data structures and tests that ive collected throughout courses
at university during my CS education, i.e bitmap.c and the like.

The second part is libooc, and is based on the following book

	www.cs.rit.edu/~ats/books/ooc.pdf

Which focuses on the techniques and primitives needed for implementing
Object Oriented programming patterns with ANSI-C.

The overall scope of this repository is to eventually combine the above
two concepts, to enable easy kickstarting of future projects I'll write
in C, as I've done with the liblcthw successfully many times already.


