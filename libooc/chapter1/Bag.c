#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "new.h"
#include "Set.h"
#include "Object.h"

/* if we dereference a pointer to a struct, we get its first value, which is its count. */
/* thus, if we cast a void * to a struct Set * set, and then assert set, we automatically check if count == 0, since assert checks if null */
/* and 0 == NULL in c. fucking voodoo man */
struct Set { unsigned count; }; //how many elements in a set?
struct Object { unsigned count; struct Set * in; }; //count is a refference count, i.e sets have ref. count. 

static const size_t _Set = sizeof(struct Set);
static const size_t _Object = sizeof(struct Object);

const void * Set = & _Set;
const void * Object = & _Object;

void * new(const void * type, ...)
{
	const size_t size = * (const size_t *) type; //cast to pointer to a size, and dereference it. now you know size to allocate
	void * p = calloc(1, size);
	assert(p);
	return p;
}

void delete(void * item)
{
	free(item);
}

void * add(void * _set, const void * _element)
{
	struct Set * set = _set;
	struct Object * element = (void *) _element;

	assert(set);
	assert(element);

	if(!element->in)
		element->in = set;

	else
		assert(element->in == set);
	++element->count, ++set->count; //increment refference count of the element, and number of elements in set. 
	return element;
}

void * find(const void * _set, const void * _element)
{
	const struct Object * element = _element;
	assert(_set);
	assert(element);
	return element->in == _set ? (void *) element : 0;
}

int contains(const void * _set, const void * _element)
{
	return find(_set,_element) != 0;
}

void * drop(void * _set, const void * _element)
{
	struct Set * set = _set;
	struct Object * element = find(set, _element);
	if(element)
	{
		if(--element->count == 0) //decrement the count. Check if its empty
			element->in = 0; //if its empty, its in none
		--set->count; //we removed regardless
	}
	return element;
}

/* do this instead of allowing an application to access .count */
unsigned count(const void * _set)
{
	const struct Set * set = _set;
	assert(set);
	return set->count;
}

int differ(const void * a, const void * b)
{
	return a != b;
}
