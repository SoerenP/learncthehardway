#include <assert.h>
#include <stdio.h>

#include "new.h"
#include "Object.h"
#include "Set.h"

#if ! defined MANY || MANY < 1
#define MANY 10
#endif

/* an object stores no information. Every object belongs to at most one set. */
/* each object can thus be represented as a small, unique, positive integer values */
/* we use these to index them into our heap. */
/* if an object is a member of a set, the objects array element containts the integer value representing the set */
/* an object therefor points to a set containing it */

const void * Set;
const void * Object;

static int heap [MANY];

/* we use 0 to designate available elements of our heap, therefor we have no set that is pointed to by 0 */
void * new (const void * type, ...)
{
	int * p;
	for(p = heap+1; p < heap + MANY; ++p)
		if(! * p)
			break;
	assert(p < heap + MANY);
	* p = MANY;
	return p;
}

/* we handle generic pointers by adding a _ before them, then we cast appropriately inside functions */
void delete (void * _item)
{
	int * item = _item;

	if(item)
	{
		assert(item > heap && item < heap + MANY);
		* item = 0;
	}
}


void * add(void * _set, const void * _element)
{
	int * set = _set;
	const int * element = _element;
	assert(set > heap && set < heap + MANY); /* the pointer to the set must be more than pointer to heap, else set cant be in heap */
	assert(* set == MANY);
	assert(element > heap && element < heap + MANY);

	if(* element == MANY) //if an element contains MANY it can be added to the set, o.w it should already be in the set
		*(int *) element = set - heap;
	else
		assert(* element == set-heap);

	return (void *) element;
}

void * find(const void * _set, const void *_element)
{
	const int * set = _set;
	const int * element = _element;

	assert(set > heap && set < heap + MANY);
	assert(* set == MANY);
	assert(element > heap && element < heap + MANY);
	assert(* element);

	return * element == set - heap ? (void *) element : 0;
}

int contains (const void * _set, const void * _element)
{
	return find(_set, _element) != 0;
}

void * drop(void * _set, const void * _element)
{
	int * element = find(_set, _element);
	if(element)
		* element = MANY;
	return element;
}

int differ(const void * a, const void * b)
{
	return a != b;
}
