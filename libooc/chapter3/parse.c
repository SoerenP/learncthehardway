#include <ctype.h>
#include <errno.h>
#include <setjmp.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "parse.h" /* defines NUMBER */
#include "value.h"

static double number; /* if token is a NUMBER: numerical value here is set by scan*/
static enum tokens token; /* current input symbol, set by scan */
static jmp_buf onError;

/* return token is next input symbol */
/* on call scan(0) we advance on the current string */
static enum tokens scan (const char * buf)
{
	static const char * bp; //static between calls, so if buf == NULL we keep going with prev. string
	if(buf)
		bp = buf; /*new input line */

	/* ignore whitespace */
	while(isspace(* bp & 0xff))
		++bp;

	if(isdigit(* bp & 0xff) || * bp == '.') //leading digit, extract str to a digit with ansi c strtod 
	{
		errno = 0;
		token = NUMBER, number = strtod(bp, (char **) & bp);
		if(errno == ERANGE)
			error("bad value: %s", strerror(errno));
	}
	else
		token = * bp ? * bp ++ : 0; //if token is not null, return it (then its an operator), if we are null terminated return 0
	return token;
}

/*
 * factor : 	+ factor
 *		- factor
 *		NUMBER
 *		(sum)
*/		


static void * sum(void);

/* as soon as a token is consumed we must call scan(0) to get the next token */
static void * factor(void)
{
	void * result;
	switch(token){
	case '+':
		scan(0);
		return factor();
	case '-':
		scan(0);
		return new(Minus, factor());
	default:
		error("bad factor: '%c' 0x%x", token, token);	
	case NUMBER:
		result = new(Value, number);
		break;
	case '(':
		scan(0);
		result = sum();
		if(token != ')')
			error("expecting )");
	}
	scan(0);
	return result;
}

/*
 * product : factor { *|/ factor }...
*/

static void * product(void)
{
	void * result = factor();
	const void * type;

	for(;;) /* keep eating till one case returns */
	{
		switch((int)token){
		case '*':
			type = Mult;
			break;
		case '/':
			type = Div;
			break;
		default:
			return result;
		}
		scan(0);
		result = new(type, result, factor());
	}
}	

static void * sum(void)
{
	void * result = product();
	const void * type;

	for(;;)
	{
		switch(token){
		case '+':
			type = Add;
			break;
		case '-':
			type = Sub;
			break;
		default:
			return result;
		}
		scan(0);
		result = new(type, result, product());
	}
}


int main(void)
{
	volatile int errors = 0;
	char buf [BUFSIZ];

	if(setjmp(onError)) //if we get an error we jump to here and increment errors
		++errors;

	while(gets(buf)) //while buffer aint empty
	{
		if(scan(buf)) //get next char, places into token.
		{
			void * e = sum();
			if(token) //zero at the end of a line
				error("trash after sum");
			process(e);
			delete(e);
		}
	}

	return errors > 0;
}

void error(const char * fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap), putc('\n', stderr);
	va_end(ap);
	longjmp(onError,1); //jump to setjmp and return 1, increment error counter;
}
