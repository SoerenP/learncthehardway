#ifndef POINT_H
#define POINT_H

extern const void * Point; /* new(point, x, y); */

void move(void * point, int dx, int dy);

#endif
