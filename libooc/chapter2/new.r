#ifndef CLASS_R
#define CLASS_R

#include <stdarg.h>
#include <stdio.h>

/* a concrete object will have a pointer to a class description like the below at the top of it, despite what object it is */
/*
	p	object	    struct class
	[*]----[*   ]------->[size]
	       [data]	     [ctor]
			     [dtor]
*/


struct Class {
	size_t size;
	void * (* ctor) (void * self, va_list * app); //pointer to a constructor
	void * (* dtor) (void * self);// destructor
	void * (* clone) (const void * self);
	int (*differ) (const void * self, const void * b);
};


#endif
