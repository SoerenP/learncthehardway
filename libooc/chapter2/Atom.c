#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "String.h"
#include "new.h"
#include "new.r"

/* all atoms with the same text are the same. */
/* i.e there exists only one atom with the string "derp" */
/* if you try to create another, you get a ref. to one with "derp" if */
/* it exists prior. */
struct String {
	const void * class; /* must be first */
	char * text;
	struct String * next;
	unsigned count;
};

/* will be maintained by dtor/ctor */
static struct String * ring; /* of all strings */

/* look in the ring of atoms after an atom with the string */
/* if we find it, return it, and remember to free what new allocated */
/* before callint ctor */
static void * String_ctor (void * _self, va_list * app)
{
	struct String * self = _self;
	const char * text = va_arg(app, const char *);

	if(ring)
	{
		struct String * p = ring;

		do
		{
			if(strcmp(p->text, text) == 0)
			{
				++p->count;
				free(self);
				return p;
			}
		} while((p = p->next) != ring);
	}
	else
		ring = self;

	/* insert us into the ring */
	self->next = ring->next, ring->next = self;
	self->count = 1; //we just made the first atom of this kind

	/* copy over the text */
	self->text = malloc(strlen(text)+1);
	assert(self->text);
	strcpy(self->text, text);
	return self;
}

/* two instances of atoms that are the same text value point to the same */
/* underlying object. So we can simply check the ref. count */
static void * String_dtor(void * _self)
{
	struct String * self = _self;

	/* decrement ref. count, if still positive, we are done */
	if(--self->count > 0)
		return 0; //return null, delete will leave us alone	

	assert(ring);
	if(ring == self)
		ring = self->next;
	if(ring == self)
		return 0; //we are the only one, but we have ref count > 0, 
	else
	{
		/* find ourseles in the ring */
		struct String * p = ring;
		while(p->next != self)
		{
			p = p->next;
			assert(p != ring);//we shouldnt NOT find us
		}
		p->next = self->next; /* remove us, link others together */
	}

	free(self->text), self->text = 0;
	return self;
}

static int String_differ(const void * self, const void * b)
{
	return self != b;
}

/* cloning means there's one more atom, so just increment the count */
static void * String_clone (const void * _self)
{
	struct String * self = (void *) _self;
	++self->count;
	return self;
}

static const struct Class _String = {
	sizeof(struct String),
	String_ctor, String_dtor,
	String_clone, String_differ
};

const void * String = & _String;
