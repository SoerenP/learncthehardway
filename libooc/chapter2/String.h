#ifndef STRING_H
#define STRING_H


/* an implementation of a String class will expose a pointer that describes the class with a Class Struct Pointer */
/* with this below, code that includes String.h and gets linked to a SPECIFIC implementation of a string */
/* i.e either String.o or Atom.o can find such a class descriptor */
extern const void * String;


#endif
