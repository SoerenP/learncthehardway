#include <assert.h>
#include <stdlib.h>

#include "new.r"
#include "new.h"

void * new (const void * _class, ...)
{
	const struct Class * class = _class; //cast void pointer input to pointer to a class description.
	void * p = calloc(1, class->size); //we know the size of the object (class) from the description

	assert(p);
	/* p points to memory area for the new object */
	/* we force it to be interpreted as pointing to a class description, and then we jam the description in there */
	* (const struct Class **) p = class;

	/* if the description has a pointer to a specific constructor, we call it now */
	if(class->ctor)
	{
		va_list ap;
		va_start(ap, _class); //if we have more arguments, get them
		p = class->ctor(p, &ap); //the start of the object has its description, so we send in p, and overwrite it.
					 //then the constructor will pad the object with data "below" the description 
		va_end(ap);
	}
	return p;
}

/* delete assumes that if a class is not null, it has a description for its dtor */
void delete (void * self)
{
	const struct Class ** cp = self; //the start of the class is a pointer to a class description pointer
	if(self && * cp && (* cp)->dtor) //if self is a function, if the class description is there, and if a destructor exists
	{
		self = (* cp)->dtor(self); //follow the pointer to the class description and call its destructor
	}
	/* all that is left after the dtor cleans up is the class description at the top of self, which we now free */
	free(self);
}

void * clone (const void * self)
{
	const struct Class * const * cp = self;
	assert(self && * cp && (* cp)->clone);
	return (* cp)->clone(self);
}


/* we assume that directly below the arbitrary self pointer, we find a type description */
/* this is late linkage, i.e only at runtime to we actually find out what methods we call */
/* or rather, dynamic linkage */
/* its a selector function, since its not dynamic in and of itself, but it dynamically calls the function doing the work */
int differ(const void * self, const void * b)
{
	const struct Class * const * cp = self;
	assert(self && * cp && (* cp)->differ);
	return (* cp)->differ(self, b);//polymorphic, accepts different classes since they all contain differ (overloading)
}

size_t sizeOf(const void * self)
{
	/* self points to an object, which at its top, has a pointer to a class description */
	const struct Class * const * cp = self;
	assert(self && * cp);
	return (* cp)->size;
}

