#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "String.h"
#include "new.r"
#include "new.h"

struct String {
	const void * class; /*class description, must be first */
	char * text;
};


/* all functions are static. As such, noone can call them (incapsulation) */
/* expect, new and delete can call them through the type descriptor! */
static void * String_ctor (void * _self, va_list * app)
{
	struct String * self = _self; //new allocated space for us.
	const char * text = va_arg(* app, const char *); //the second argument in the var list is assumed to be our text
	self->text = malloc(strlen(text)+1);
	assert(self->text);
	strcpy(self->text, text);
	return self;
}

/* delete checks that _self is not null so we do not need to check this */
static void * String_dtor (void * _self)
{
	struct String * self = _self;
	free(self->text), self->text = 0;
	return self;
}

static void * String_clone (const void * _self)
{
	const struct String * self = _self;
	return new(String, self->text);
}

static int String_differ (const void * _self, const void * _b)
{
	const struct String * self = _self;
	const struct String * b = _b;

	if(self == b) //is it in fact the same object?
		return 0;

	if(!b || b->class != String) //is second argument really a string?
		return 1;
	return strcmp(self->text, b->text); //both string objects, do they have the same data? 
}

/* the type descriptor, its the same for all strings. ITS NOT A STRING CLASS, but the descriptor for it */
static const struct Class _String = {
	sizeof(struct String),
	String_ctor, String_dtor,
	String_clone, String_differ
};

const void * String = & _String; //Heres the pointer we will use for our string description. 
