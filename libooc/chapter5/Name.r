#ifndef NAME_R
#define NAME_R

struct Name {	
	const void * type; /* for dynamic linkage */
	const char * name; /* may be malloced */
	int token;
};

#endif
