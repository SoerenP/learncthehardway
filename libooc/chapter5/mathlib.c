#include <assert.h>
#include <errno.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "Name.h"
#include "Name.r"
#include "mathlib.h"
#include "parse.h"
#include "value.h"
#include "value.r"

/*
 *
 * libm(3) functions
 * tree->struct Bin
 * 	type: math
 *	left -> struct math for sin, etc
 *	right -> argument tree
*/

struct Math { struct Name _;
	double (* funct) (double);
};

#define funct(tree) (((struct Math *) left(tree))->funct)

static void freeMath (void * tree)
{
	delete(right(tree));//right subtree is a sum, so delete it. 
	free(tree);
}

static double doMath(const void * tree)
{
	/* the sum (argument) to the function must be deciphered first */
	double result = exec(right(tree));

	errno = 0;
	/* take the function saved in the math node and call it */
	result = funct(tree)(result);

	/* we try to catch numerical errors, but errno.h takes care of that since we call */
	/* functions from stdlib (math.h) */
	if(errno)
		error("error in %s: %s",
		((struct Math *) left(tree))->_.name,
		strerror(errno));
	return result;
}

static const struct Type _Math = {mkBin, doMath, freeMath };
const void * Math = &_Math;


/* we just take the functions from <math.h> and stuff into symbol table */
void initMath(void)
{
	static const struct Math functions [] = {
		{{&_Math, "sin", MATH }, sin },
		{{&_Math, "cos", MATH }, cos },
		{{&_Math, "tan", MATH }, tan },
		{{&_Math, "asin", MATH}, asin },
		{{&_Math, "acos", MATH}, acos },
		{{&_Math, "atan", MATH}, atan },
		{{&_Math, "sinh", MATH}, sinh },
		{{&_Math, "cosh", MATH}, cosh },
		{{&_Math, "tanh", MATH}, tanh },
		{{&_Math, "exp", MATH}, exp },
		{{&_Math, "log", MATH}, log },
		{{&_Math, "log10", MATH}, log10},
		{{&_Math, "sqrt", MATH}, sqrt },
		{{&_Math, "ceil", MATH}, ceil },
		{{&_Math, "floor", MATH}, floor },
		{{&_Math, "abs", MATH}, fabs },
		{{0}}}; //simply to stop below for loop 

	const struct Math * mp;
	for(mp = functions; mp->_.name; ++mp)
		install(mp);
}
