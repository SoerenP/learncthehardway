#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "Name.h"
#include "Name.r"
#include "parse.h"
#include "Var.h"
#include "value.h"
#include "value.r"


/*
 * variables
 * tree->struct Var (also located in symbol table)
 * Subclass of name
*/

struct Var { struct Name _; double value; };

#define value(tree) (((struct Var *) tree)->value)

static void * mkVar(va_list ap)
{
	struct Var * node = calloc(1, sizeof(struct Var));
	const char * name = va_arg(ap, const char *);
	size_t len = strlen(name);

	assert(node);

	node->_.name = malloc(len+1);
	assert(node->_.name);
	strcpy((void *) node->_.name, name);
	node->_.token = VAR;
	return node;
}


static double doVar(const void * tree)
{
	return value(tree);
}

/* we never delete variables - we might delete the var expression they are in */
/* but they MUST stay in the symbol table, since a Var can be used over and over */
static void freeVar(void * tree)
{
}


/* assignment */
/* resuses strut Bin, mkBin() */
/* we are guaruanteed a var on the left and sum on right, so just */
/* do the  assignment to the value of the var */
static double doAssign(const void * tree)
{
	return value(left(tree)) = exec(right(tree));
}

static struct Type _Var = { mkVar, doVar, freeVar };
static struct Type _Assign = { mkBin, doAssign, freeBin };
const void * Var = & _Var;
const void * Assign = & _Assign;

/*
 * Constants
 * like variables, but with a distinc token
 * so the first Part is a Name (string and token) and the Var is then a name with a value 
 * factor() will not allow CONST to be on the left hand side 
*/

void initConst(void)
{
	static const struct Var constants [] = { 
		{ { &_Var, "PI", 	CONST }, 3.14159265358979323846},
		{ { &_Var, "E", 	CONST }, 2.71828182845904523536},
		{ { &_Var, "GAMMA", 	CONST }, 0.57721566490153286060},
		{ { &_Var, "DEG",	CONST }, 57.29577951398232087680},
		{ { &_Var, "PHI", 	CONST }, 1.61803398874989484829},
		{ { 0} } }; //simply a zero for the below for loop to find 
	const struct Var * vp;
	for(vp = constants; vp->_.name; ++vp)
	{
		printf("installing");
		install(vp);
	}
}
