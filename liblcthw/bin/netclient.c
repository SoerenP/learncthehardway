#undef NDEBUG
#include <stdlib.h>
#include <sys/select.h>
#include <stdio.h>
#include <lcthw/ringbuffer.h>
#include <lcthw/dbg.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>

struct tagbstring NL = bsStatic("\n");
struct tagbstring CRLF = bsStatic("\r\n");

/* get the flags of the file descriptor, then set the file descriptor */
/* to be what it was but also nonblocking */
int nonblock(int fd)
{
	int flags = fcntl(fd, F_GETFL, 0);
	check(flags >= 0, "Invalid flags on nonblock.");

	int rc = fcntl(fd, F_SETFL, flags | O_NONBLOCK);
	check(rc == 0, "Can't set nonblocking.");

	return 0;
error:
	return -1;
}

/* connect to host, on port */
/* using ipv4 = AF_INET     */
/* then set it nonblocking  */
int client_connect(char *host, char *port)
{
	check((host != NULL) && (port != NULL), "Recieved invalid pointers");

	int rc = 0;
	struct addrinfo *addr = NULL;

	rc = getaddrinfo(host, port, NULL, &addr);
	check(rc == 0, "Failed to lookup %s:%s",host,port);

	int sock = socket(AF_INET, SOCK_STREAM,0);
	check(sock >= 0, "Cannot create a socket.");

	rc = connect(sock,addr->ai_addr, addr->ai_addrlen);
	check(rc == 0, "Connect failed.");

	rc = nonblock(sock);
	check(rc == 0, "Can't set nonblocking.");

	freeaddrinfo(addr);
	return sock;	
error:
	freeaddrinfo(addr);
	return -1;
}

/* reads as much as it can from socket/file into ringbuffer */
int read_some(RingBuffer *buffer, int fd, int is_socket)
{
	check(buffer != NULL, "Recieved NULL pointer.");

	int rc = 0;

	if(RingBuffer_available_data(buffer) == 0){
		buffer->start = buffer->end = 0;
	}

	if(is_socket){
		rc = recv(fd, RingBuffer_starts_at(buffer), RingBuffer_available_space(buffer),0);
	} else {
		rc = read(fd, RingBuffer_starts_at(buffer), RingBuffer_available_space(buffer));
	}

	check(rc >= 0, "Failed to read from fd: %d", fd);

	RingBuffer_commit_write(buffer, rc);

	return rc;
error:
	return -1;
}

/* sends/writes as much as it can from ringbuffer to file/socket */
int write_some(RingBuffer *buffer, int fd, int is_socket, int rflag)
{
	check(buffer != NULL, "Recieved NULL pointer.");

	int rc = 0;
	bstring data = RingBuffer_get_all(buffer);


	/* replaced newline (unix) with CRLF (windows) */
	check(data != NULL, "Failed to get from the buffer.");
	if(rflag){
		check(bfindreplace(data, &NL, &CRLF, 0) == BSTR_OK, "Failed to replace NL.");
	}

	if(is_socket){
		rc = send(fd, bdata(data), blength(data), 0);
	} else {
		rc = write(fd, bdata(data), blength(data));
	}

	check(rc == blength(data), "Failed to write everything to fd: %d.", fd);
	bdestroy(data);

	return rc;
error:
	return -1;
}

int main(int argc, char *argv[])
{

	int rflag = 0;
	int c;
	char *host = NULL;
	char *port = NULL;


	while((c = getopt(argc,argv,"h:p:r:")) != -1)
	{
		switch(c)
		{
			case 'h':
				host = optarg;
				break;
			case 'p':
				port = optarg;
				break;	
			case 'r':
				rflag = atoi(optarg);
				break;
			case '?':
				break;
				
		}
	}

	fd_set allreads;
	fd_set readmask;

	int socket = 0;
	int rc = 0;
	RingBuffer *in_rb = RingBuffer_create(1024*10);
	RingBuffer *sock_rb = RingBuffer_create(1024*10);

	check(argc >= 3, "USAGE: netclient -h host -p port -r rflags");

	socket = client_connect(host, port);
	check(socket >= 0, "connect to %s:%s failed.", host, port);

	FD_ZERO(&allreads);
	FD_SET(socket, &allreads);
	FD_SET(0, &allreads);

	while(1){
		readmask = allreads;
		rc = select(socket + 1, &readmask, NULL, NULL, NULL);
		check(rc >= 0, "select() failed.");

		if(FD_ISSET(0, &readmask)){
			rc = read_some(in_rb, 0, 0);
			check_debug(rc != -1, "Failed to read from socket.");
		}


		if(FD_ISSET(socket, &readmask)){
			rc = read_some(sock_rb, socket, 0);
			check_debug(rc != -1, "Failed to read from socket");
		}

		while(!RingBuffer_empty(sock_rb)){
			rc = write_some(sock_rb,1,0,rflag);
			check_debug(rc != -1, "Failed to write to stdout.");
		}

		while(!RingBuffer_empty(in_rb)){
			rc = write_some(in_rb, socket, 1, rflag);
			check_debug(rc != -1, "Failed to write to socket.");
		}
	}

	return 0;
error:
	return -1;
}
