#include <lcthw/minunit.h>
#include <lcthw/bitmap.h>
#include <time.h>
#include <assert.h>

#define DEFAULT_BITMAP_SIZE 1000

static struct Bitmap *map = NULL;
static struct Bitmap *random_map = NULL;
static struct Bitmap *map_to_integer = NULL;
static int countOfOnesInMap = 0;

char *test_create()
{

	map = Bitmap_create(DEFAULT_BITMAP_SIZE);
	mu_assert(map != NULL, "Failed to create bitmap.");

	random_map = Bitmap_create_random(DEFAULT_BITMAP_SIZE);
	mu_assert(random_map != NULL, "Failed to create random bitmap.");

	map_to_integer = Bitmap_create(DEFAULT_BITMAP_SIZE);
	mu_assert(map_to_integer != NULL, "Failed to create bitmap.");

	return NULL;
}

char *test_destroy()
{
	Bitmap_free(map);
	Bitmap_free(random_map);
	Bitmap_free(map_to_integer);
	return NULL;
}

char *test_set__clear_bitmap()
{
	int index1 = DEFAULT_BITMAP_SIZE/2;
	int index2 = DEFAULT_BITMAP_SIZE/3;

	Bitmap_set(map, index1);
	mu_assert(Bitmap_test(map, index1) == 1, "Failed to set bit index.");

	Bitmap_set(map, index2);
	mu_assert(Bitmap_test(map, index2) == 1, "Failed to set bit index.");

	Bitmap_clear(map,index1);
	mu_assert(Bitmap_test(map,index1) == 0, "Failed to clear bit index.");

	Bitmap_clear(map,index2);
	mu_assert(Bitmap_test(map,index2) == 0, "Failed to clear bit index.");
	
	return NULL;
}

char *test_rank_bitmap()
{
	int index = 0;
	int ones = 0;
	for(index = 0; index < map->bits; index++)
	{
		if(rand() % 2 == 0) //if we get an even number, set the bit
		{
			Bitmap_set(map, index);
			ones++;
		}
	}

	int rank = Bitmap_naive_rank(map, map->bits);
	mu_assert(rank == ones, "Naive rank gave wrong result");

	countOfOnesInMap = rank;

	return NULL;
}

char *test_select_bitmap()
{
	int index = 0;
	int middle_one = countOfOnesInMap / 2; //we want to find the index of a one we know is there.
	int ones_encountered = 0;

	//keep counting and incrementing index until youve met half the ones
	while(ones_encountered < middle_one)
	{
		if(Bitmap_test(map,index) == 1)
			ones_encountered++;

		index++;
	}
	
	//compare to select up until that index
	mu_assert(Bitmap_naive_select(map,middle_one) == index,"Naive select failed");

	return NULL;
}

char *test_bitmap_to_int()
{
	//set up a bitmap that should map to a known integer in some subset
	//00101010 is 42
	//but the bitmap indexing is from -> 00...000 left to right
	int expected = 42;

	Bitmap_set(map_to_integer,1);
	Bitmap_set(map_to_integer,3);
	Bitmap_set(map_to_integer,5);

	int result = fromBitmapToInt32Const(map_to_integer,0,map_to_integer->bits);
	mu_assert(result == expected, "Failed to map integer correctly");

	return NULL;
}


char *all_tests()
{
	mu_suite_start();
	srand(time(NULL));
	mu_run_test(test_create);

	mu_run_test(test_set__clear_bitmap);
	mu_run_test(test_rank_bitmap);
	mu_run_test(test_select_bitmap);
	mu_run_test(test_bitmap_to_int);

	mu_run_test(test_destroy);

	return NULL;
}

RUN_TESTS(all_tests);
