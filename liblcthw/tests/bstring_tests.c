#include <lcthw/minunit.h>
#include <lcthw/bstrlib.h>
#include <string.h>

char *test_bfromcstr()
{
	const char *test_string = "Hello";
	bstring b = bfromcstr("Hello");
	mu_assert(b != NULL, "Failed to create b string.");

	mu_assert(strcmp((const char *)b->data,test_string)==0,"Strings should be equal.");

	if(b) bdestroy(b);
	return NULL;
}

char *test_blk2bstr()
{
	const void *blk = "ImAPrettyFlower";
	int len = 15;

	bstring b = blk2bstr(blk,len);
	mu_assert(b != NULL, "Failed to create b string.");
	mu_assert(blength(b) == 15, "Resulting b string has incorrect length.");


	mu_assert(strcmp((const char *)b->data, (const char *)blk)==0, "Strings should be equal.");

	const void *blk_other = "ImTryingToTest";
	bassigncstr(b,(char *)blk_other);

	mu_assert(strcmp((const char *)b->data, (const char *)blk_other)==0, "Strings should be equal.");

	const void *blk_with_len = "TellMeYourSize!";
	
	bassignblk(b, blk_with_len, len);
	mu_assert(strcmp((const char *)b->data, (const char *)blk_with_len)==0, "Strings should be equal.");

	if(b) bdestroy(b);

	return NULL;
}

char *test_bstrcpy()
{

	bstring b = bfromcstr("UggaBugga");
	mu_assert(b != NULL, "Failed to create bstring.");

	bstring b_copy = bstrcpy(b);
	mu_assert(b_copy != NULL, "Failed to copy bstring.");

	mu_assert(strcmp((char *)b->data, (char *)b_copy->data)==0, "Original and Copy are different");

	bstring b_other = bfromcstr("BuggaUgga");
	mu_assert(b_other != NULL, "Failed to create bstring.");

	unsigned char split = 12;
	struct bstrList *bstr_list = bsplit(b_other, split);
	mu_assert(bstr_list != NULL, "Failed to create bstrList");

	if(b) bdestroy(b);
	if(b_copy) bdestroy(b_copy);
	if(bstr_list) bstrListDestroy(bstr_list);
	if(b_other) bdestroy(b_other);

	return NULL;
}

char *test_bstr_manipulation()
{
	bstring b0 = bfromcstr("World");
	mu_assert(b0 != NULL, "Failed to create b string");
	bstring b1 = bfromcstr("Hello");
	mu_assert(b1 != NULL, "Failed to create b string");	

	int rc = bconcat(b1,b0);
	mu_assert(BSTR_OK == rc, "Failed to concatenate");

	const char *cstr = "HelloWorld";
	mu_assert(strcmp((char *)b1->data, cstr)==0, "Strings should be equal.");
	bstring b2 = bfromcstr(cstr);
	mu_assert(b2 != NULL, "Failed to create b string");

	rc = bstricmp(b2,b1);
	mu_assert(rc == 0, "Strings should be equal.");	

	rc = biseq(b2,b1);
	mu_assert(rc == 1, "Strings should be equal.");

	rc = binstr(b2, 0, b0);
	mu_assert(rc == 5, "String should contain given substring.");

	const char *replacement = "Søren";
	const char *find = "World";
	bstring b_find = bfromcstr(find);
	mu_assert(b_find != NULL, "Failed to create b string");
	bstring b_replacement = bfromcstr(replacement);
	mu_assert(b_replacement != NULL, "Failed to create b string");

	bfindreplace(b2, b_find, b_replacement, 0);
	mu_assert(strcmp((char *)b2->data, "HelloSøren") == 0, "Strings should be eqaul.");

	if(b0) bdestroy(b0);
	if(b1) bdestroy(b1);	
	if(b2) bdestroy(b2);
	if(b_find) bdestroy(b_find);
	if(b_replacement) bdestroy(b_replacement);

	return NULL;
}

char *test_format()
{
	const char *name = "Søren";
	const char *expected_result = "Hello Søren";
	bstring b = bformat("Hello %s",name);
	mu_assert(b != NULL, "Failed to create bstring.");

	mu_assert(strcmp((char *)b->data, expected_result)==0, "Failed to format string");

	const char *res_macro = bdata(b);
	mu_assert(strcmp(res_macro,expected_result)==0, "Failed to get data using bdata.");

	const char expected_char = 'H';
	const char actual_char = bchar(b,0);
	mu_assert(expected_char == actual_char,"Failed to get char using bchar.");

	if(b) bdestroy(b);

	return NULL;
}
/* all dl function tests must be run in the specified order, to fecth */
/* function pointers to the actual functions form the dynamically */
/* library */
char *all_tests()
{
	mu_suite_start();
	mu_run_test(test_bfromcstr);
	mu_run_test(test_blk2bstr);
	mu_run_test(test_bstrcpy);
	mu_run_test(test_bstr_manipulation);
	mu_run_test(test_format);

	return NULL;
}

RUN_TESTS(all_tests);
