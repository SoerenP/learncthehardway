#include <lcthw/minunit.h>
#include <lcthw/ringbuffer.h>
#include <lcthw/bstrlib.h>

#define BUFFER_SIZE 30
#define ERROR_CODE -1

static RingBuffer *buffer = NULL;

char *test_create()
{
	buffer = RingBuffer_create(BUFFER_SIZE);
	mu_assert(buffer != NULL, "Failed to create ringbuffer.");

	return NULL;
}

char *test_write_read()
{
	char *msg = "Test1";
	int msg_length = 5;

	int rc = RingBuffer_write(buffer,msg,msg_length);
	mu_assert(rc == msg_length, "write returned wrong length.");

	char msg_read[BUFFER_SIZE] = {0};
	rc = 0;
	rc = RingBuffer_read(buffer, msg_read, msg_length);
	mu_assert(rc == msg_length, "read returned the wrong length.");	
	
	return NULL;
}

char *test_gets()
{
	char *msg = "Bstring wanted";
	int msg_length = 14;

	int rc = RingBuffer_write(buffer,msg,msg_length);
	mu_assert(rc == msg_length, "write returned wrong length.");
	
	bstring test_bstr = RingBuffer_gets(buffer,msg_length);
	mu_assert(blength(test_bstr) == msg_length,
		"Failed to get bstring of appropriate length.");

	bstring expected = bfromcstr("Bstring wanted");
	rc = bstricmp(expected,test_bstr);
	mu_assert(rc == 0, "Bstring gotten has incorrect content.");

	bdestroy(expected);
	bdestroy(test_bstr);

	return NULL;
}

char *test_fill_up()
{
	char *msg = "theroeifhtjelkef";
	int msg_length = 16;

	int rc = RingBuffer_write(buffer,msg,msg_length);
	mu_assert(rc == msg_length, "Failed to write to buffer.");
	
	char msg_read[16] = {0};
	rc = 0;
	rc = RingBuffer_read(buffer,msg_read,msg_length);
	mu_assert(rc == msg_length, "Failed to read the buffer.");

	return NULL;
}

char *test_small_increments()
{
	char *msg = "k";
	int msg_length = 1;
	int index = 0;
	int rc = 0;
	for(index = 0; index < BUFFER_SIZE-1; index++)
	{
		rc = 0;
		rc = RingBuffer_write(buffer,msg,msg_length);
		mu_assert(rc == msg_length, "Failed to write to buffer.");
	}
	mu_assert(RingBuffer_available_data(buffer) == BUFFER_SIZE-1, 
		"The buffer should be full.");

	char *msg_to_read = malloc(sizeof(char)*BUFFER_SIZE+1);

	for(index = 0; index < BUFFER_SIZE-1; index++)
	{
		rc = 0;
		rc = RingBuffer_read(buffer,msg_to_read,msg_length);
		mu_assert(rc == msg_length, "Failed to read from buffer.");
	}
	mu_assert(RingBuffer_available_data(buffer) == 0,
		"The buffer should be empty.");

	free(msg_to_read);

	return NULL;
}


char *test_destroy()
{
	RingBuffer_destroy(buffer);

	return NULL;
}



char *all_tests()
{
	mu_suite_start();

	mu_run_test(test_create);
	mu_run_test(test_write_read);
	mu_run_test(test_gets);
	mu_run_test(test_fill_up);
	mu_run_test(test_small_increments);

	mu_run_test(test_destroy);

	return NULL;
}

RUN_TESTS(all_tests);
