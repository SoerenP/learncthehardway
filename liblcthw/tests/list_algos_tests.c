#include <lcthw/minunit.h>
#include <lcthw/list_algos.h>
#include <assert.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

char **values = NULL;
#define NUM_VALUES 10000
#define BLOCK_SIZE 4

void rand_str(char *dest, size_t length)
{
	char charset[] = "0123456789abcdefghijklmnopqrstuvxzyABCDEFGHIJKLMOPQRTSYVXZY";
	int index = 0;
	for(index = 0; index < (int)length; index++)
	{
		dest[index] = charset[(int)rand() % sizeof(charset -1)];
	}
	dest[length-1] = '\0';
}



char *setup_values()
{
	values = (char **)malloc(sizeof(char *) * (NUM_VALUES));
	mu_assert(values != NULL, "Failed to set up values");
	
	int index = 0;
	for(index = 0; index < NUM_VALUES; index++)
	{
		values[index] = (char *)malloc(BLOCK_SIZE+1);
		rand_str(values[index],BLOCK_SIZE+1);
	}

	return NULL;
}

char *destroy_values()
{
	mu_assert(values != NULL, "Nothing to free, already NULL");
	int index = 0;
	if(values){
		for(index = 0; index < NUM_VALUES; index++)
		{
			if(values[index])
				free(values[index]);
		}
		free(values);
	}
	return NULL;
}

List *create_words()
{	
	int i = 0;
	List *words = List_create();

	for(i = 0; i < NUM_VALUES; i++){
		List_push(words, values[i]);
	}
	return words;
}

int is_sorted(List *words)
{
	LIST_FOREACH(words, first, next, cur){
		if(cur->next && strcmp(cur->value, cur->next->value) > 0){
			debug("%s %s", (char *)cur->value, (char *)cur->next->value);
			return 0;
		}
	}
	return 1;
}

char *test_bubble_sort()
{
	List *words = create_words();
	
	//should work on a list that needs sorting
	int rc = List_bubble_sort(words, (List_compare)strcmp);
	mu_assert(rc == 0, "Bubble sort failed.");
	mu_assert(is_sorted(words), "Words are not sorted after bubble sort.");

	//should work on an already sorted list
	rc = List_bubble_sort(words, (List_compare)strcmp);
	mu_assert(rc == 0, "Bubble sort of already sorted failed.");
	mu_assert(is_sorted(words), "Words should be sort if already bubble sorted.");

	List_destroy(words);

	//should work on empty list
	words = List_create(words);
	rc = List_bubble_sort(words, (List_compare)strcmp);
	mu_assert(rc == 0, "Bubble sort failed on empty list.");
	mu_assert(is_sorted(words), "Words should be sorted if empty.");

	List_destroy(words);

	return NULL;
}

char *test_merge_sort()
{

	List *words = create_words();

	//should work on a list that needs sorting
	List *res = List_merge_sort(words, (List_compare)strcmp);
	mu_assert(is_sorted(res), "Words are not sorted after merge sort.");

	List *res2 = List_merge_sort(res, (List_compare)strcmp);
	mu_assert(is_sorted(res), "Should still be sorted after merge sort.");

	List_destroy(res2);
	List_destroy(res);	

	List_destroy(words);
	return NULL;
}

char *test_merge_vs_bubble_sort_time()
{
	clock_t begin_merge, end_merge, begin_bubble, end_bubble;
	double time_spent_merge, time_spent_bubble;

	/* setup arrays to sort */
	List *words = create_words();

	/* start time */
	begin_merge = clock();

	/* do work */
	List *merge_res = List_merge_sort(words, (List_compare)strcmp);

	/* stop time */
	end_merge = clock();

	/* should flush cache here */
	List_destroy(merge_res);
	List_destroy(words);

	/* setup arrays to sort */
	words = create_words();
	
	/* start time */
	begin_bubble = clock();

	/*do work */
	List_bubble_sort(words, (List_compare)strcmp);

	/* stop time */
	end_bubble = clock();

	/* calculate time */
	time_spent_merge = (double)(end_merge-begin_merge) / CLOCKS_PER_SEC;
	time_spent_bubble = (double)(end_bubble-begin_bubble) / CLOCKS_PER_SEC;

	printf("merge: %f  ----  bubble: %f\n",time_spent_merge,time_spent_bubble);

	List_destroy(words);
	return NULL;
}

//They must run in the correct order! Code horror, i know, but static variables and opening the library and stuff.
char *all_tests()
{
	mu_suite_start();
	mu_run_test(setup_values);
	mu_run_test(test_bubble_sort);
	mu_run_test(test_merge_sort);
	mu_run_test(test_merge_vs_bubble_sort_time);
	mu_run_test(destroy_values);

	return NULL;
}

RUN_TESTS(all_tests);
