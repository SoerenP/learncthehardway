#include <lcthw/minunit.h>
#include <lcthw/stats.h>
#include <math.h>

const int NUM_SAMPLES = 10;
double samples[] = {
	6.1061334, 9.6783204, 1.2747090, 8.2395131, 0.3333483,
    6.9755066, 1.0626275, 7.6587523, 4.9382973, 9.5788115
};

Stats expect = {
	.sumsq = 425.1641,
	.sum = 55.84602,
	.min = 0.333,
	.max = 9.678,
	.n = 10
};
double expect_mean = 5.584602;
double expect_stddev = 3.547868;

#define EQ(X,Y,N) (round((X) * pow(10, N)) == round((Y) * pow(10, N)))
#define NUMBERS_OF_PREC 2

char *test_operations()
{
	int i = 0;
	Stats *st = Stats_create();
	mu_assert(st != NULL, "Failed to create stats.");

	for(i = 0; i < NUM_SAMPLES; i++){
		Stats_sample(st, samples[i]);
	}

	Stats_dump(st);

	mu_assert(EQ(st->sumsq, expect.sumsq, NUMBERS_OF_PREC), "sumsq not valid.");
	mu_assert(EQ(st->sum, expect.sum, NUMBERS_OF_PREC), "sum not valid.");
	mu_assert(EQ(st->min, expect.min, NUMBERS_OF_PREC), "min not valid.");
	mu_assert(EQ(st->max, expect.max, NUMBERS_OF_PREC), "max not valid.");
	mu_assert(EQ(st->n, expect.n, NUMBERS_OF_PREC), "n not valid.");
	mu_assert(EQ(expect_mean, Stats_mean(st),NUMBERS_OF_PREC), "mean not valid.");
	printf("expect: %f, stddev: %f\n",expect_stddev,Stats_stddev(st));
	mu_assert(EQ(expect_stddev, Stats_stddev(st), NUMBERS_OF_PREC), "stddev not valid.");

	Stats_destroy(st);

	return NULL;
}

char *test_recreate()
{
	Stats *st = Stats_recreate(expect.sum, expect.sumsq, expect.n, expect.min, expect.max);

	mu_assert(st->sum == expect.sum, "sum not equal.");
	mu_assert(st->sumsq == expect.sumsq, "sumsq not equal.");
	mu_assert(st->min == expect.min, "min not equal.");
	mu_assert(st->max == expect.max, "max not equal.");
	mu_assert(st->n == expect.n, "n not equal.");
	mu_assert(EQ(expect_mean, Stats_mean(st),NUMBERS_OF_PREC), "mean not valid");
	mu_assert(EQ(expect_stddev,Stats_stddev(st),NUMBERS_OF_PREC), "stddev not valid.");

	Stats_destroy(st);

	return NULL;
}

char *all_tests()
{
	mu_suite_start();

	mu_run_test(test_operations);
	mu_run_test(test_recreate);

	return NULL;
}

RUN_TESTS(all_tests);
