#include <lcthw/minunit.h>
#include <lcthw/list.h>
#include <assert.h>

static List *list = NULL;
static List *copy = NULL;
static List *join = NULL;
char *test1 = "test1 data";
char *test2 = "test2 data";
char *test3 = "test3 data";

char *test_create()
{
	list = List_create();
	mu_assert(list != NULL, "Failed to create list.");

	return NULL;
}

char *test_destroy()
{
	List_clear_destroy(list);
	List_clear_destroy(copy);
	List_clear_destroy(join);
	return NULL;
}

char *test_push_pop()
{
	List_push(list, test1);
	mu_assert(List_last(list) == test1, "Wrong last value");

	List_push(list, test2);
	mu_assert(List_last(list) == test2, "Wrong last value");

	List_push(list, test3);
	mu_assert(List_last(list) == test3, "Wrong last value");
	mu_assert(List_count(list) == 3, "Wrong count on push");

	char *val = List_pop(list);
	mu_assert(val == test3, "Wrong value on pop.");

	val = List_pop(list);
	mu_assert(val == test2, "Wrong value on pop.");

	val = List_pop(list);
	mu_assert(val == test1, "Wrong value on pop.");
	mu_assert(List_count(list) == 0, "Wrong count after pop.");
	
	return NULL;
}

char *test_copy()
{
	List_push(list, test1);
	mu_assert(List_last(list) == test1, "Wrong last value");

	List_push(list, test2);
	mu_assert(List_last(list) == test2, "Wrong last value");

	List_push(list, test3);
	mu_assert(List_last(list) == test3, "Wrong last value");
	mu_assert(List_count(list) == 3, "Wrong count on push");

	copy = List_copy(list);
	mu_assert(copy != NULL, "Failed to allocate to copy list");
	mu_assert(List_last(list) == List_last(copy), "Not same last element");

	char *val_orig = List_pop(list);
	char *val_copy = List_pop(copy);
	mu_assert(val_orig == test3 && val_copy == test3, "Failed to pop from both lists");
	
	val_orig = List_pop(list);
	val_copy = List_pop(copy);
	mu_assert(val_orig == test2 && val_copy == test2, "Failed to pop from both lists");

	val_orig = List_pop(list);
	val_copy = List_pop(copy);
	mu_assert(val_orig == test1 && val_copy == test1, "Failed top pop from both lists");

	mu_assert(List_count(list) == 0 && List_count(copy) == 0, "Incorrect count after popping");

	return NULL;
}

char *test_join()
{
	List_push(list, test1);
	mu_assert(List_last(list) == test1, "Wrong last value");
	
	List_push(copy, test2);
	mu_assert(List_last(copy) == test2, "Wrong last value");

	List_push(copy, test3);
	mu_assert(List_last(copy) == test3, "Wrong last value");

	join = List_join(list,copy);
	mu_assert(join != NULL, "Failed to allocate to join list");

	char *val_orig = List_pop(copy);
	char *val_join = List_pop(join);
	mu_assert(val_orig == test3 && val_join == test3, "Failed to pop from both lists");

	val_orig = List_pop(copy);
	val_join = List_pop(join);
	mu_assert(val_orig == test2 && val_join == test2, "Failed to pop from both lists");

	val_orig = List_pop(list);
	val_join = List_pop(join);
	mu_assert(val_orig == test1 && val_join == test1, "Failed to pop from both lists");

	mu_assert(List_count(list) == 0 && List_count(copy) == 0 && List_count(join) == 0, "Incorrect count after popping");

	return NULL;
}

char *test_unshift()
{
	List_unshift(list, test1);
	mu_assert(List_first(list) == test1, "Wrong first value");

	List_unshift(list, test2);
	mu_assert(List_first(list) == test2, "Wrong first value");

	List_unshift(list, test3);
	mu_assert(List_first(list) == test3, "Wrong first value");
	mu_assert(List_count(list) == 3, "Wrong count on unshift");

	return NULL;
}

char *test_remove()
{
	//we only need to test the middle remove case since push/shift
	//already tests the other cases

	char *val = List_remove(list, list->first->next);
	mu_assert(val == test2, "Wrong removed element.");
	mu_assert(List_count(list) == 2, "Wrong count after remove.");
	mu_assert(List_first(list) == test3,  "Wrong first after remove.");
	mu_assert(List_last(list) == test1, "Wrong last after remove.");

	return NULL;
}

char *test_shift()
{
	mu_assert(List_count(list) != 0, "Wrong count before shift.");

	char *val = List_shift(list);
	mu_assert(val == test3, "Wrong value on shift.");

	val = List_shift(list);
	mu_assert(val == test1, "Wrong value on shift");
	mu_assert(List_count(list) == 0, "Wrong count after shift.");

	return NULL;
}

char *all_tests()
{
	mu_suite_start();

	mu_run_test(test_create);
	mu_run_test(test_push_pop);
	mu_run_test(test_copy);
	mu_run_test(test_join);
	mu_run_test(test_unshift);
	mu_run_test(test_remove);
	mu_run_test(test_shift);
	mu_run_test(test_destroy);
	return NULL;
}

RUN_TESTS(all_tests);
