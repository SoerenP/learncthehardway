#include <lcthw/minunit.h>
#include <lcthw/string_algos.h>
#include <lcthw/bstrlib.h>
#include <time.h>

#define PERFORMANCE_ITER 1000
#define WORDS_IN_STR 8

struct tagbstring IN_STR = bsStatic("I have ALPHA beta ALPHA and ORANGES ALPHA");
struct tagbstring ALPHA = bsStatic("ALPHA");
const int TEST_TIME = 2;

static const char *global_list[WORDS_IN_STR] = {0};
static bstring global_bstrings[WORDS_IN_STR] = {0};

char *test_find_and_scan()
{
	StringScanner *scan = StringScanner_create(&IN_STR);
	mu_assert(scan != NULL, "Failed to make Scanner.");

	int find_i = String_find(&IN_STR, &ALPHA);
	mu_assert(find_i > 0, "Failed to find 'ALPHA' in test string.");

	int scan_i = StringScanner_scan(scan, &ALPHA);
	mu_assert(scan_i > 0, "Failed to ind 'ALPHA' in test string.");
	mu_assert(scan_i == find_i, "Find and scan don't match");

	scan_i = StringScanner_scan(scan, &ALPHA);
	mu_assert(scan_i > find_i, "Should find another ALPHA after the first.");

	scan_i = StringScanner_scan(scan, &ALPHA);
	mu_assert(scan_i > find_i, "Should find another ALPHA after the first.");

	mu_assert(StringScanner_scan(scan, &ALPHA) == -1, "Shouldn't find it");

	StringScanner_destroy(scan);

	return NULL;
}

char *setup_bstrlist()
{	
	bstring b = &IN_STR;
	mu_assert(b != NULL, "Failed to get the IN_STR");

	unsigned char spaces = b->data[1]; //I dont know what the unsigned char for whitespace is. So i just take it from the bstring
	struct bstrList *str_list = bsplit(b,spaces);
	mu_assert(str_list != NULL, "Failed to split string");

	int i = 0;
	for(i = 0; i < str_list->qty; i++)
	{
		global_list[i] = str_list->entry[i]->data;
		global_bstrings[i] = bfromcstr(global_list[i]);
		mu_assert(global_bstrings[i] != NULL, "Failed to create bstring.");
	}

	bstrListDestroy(str_list);	

	return NULL;
}

bstring get_random_bstr()
{
	return global_bstrings[rand() % WORDS_IN_STR];
}

char *destroy_global_bstrings()
{
	int i = 0;
	for(i = 0; i < WORDS_IN_STR; i++)
	{
		bdestroy(global_bstrings[i]);
	}

	return NULL;
}

char *test_binstr_performance()
{
	int i = 0;
	int found_at = 0;
	unsigned long find_count = 0;
	time_t elapsed = 0;
	time_t start = time(NULL);

	do {
		for(i = 0; i < PERFORMANCE_ITER; i++)
		{
			found_at = binstr(&IN_STR, 0, get_random_bstr());
			mu_assert(found_at != BSTR_ERR, "Failed to find!");
			find_count++;
		}

		elapsed = time(NULL) - start;

	} while(elapsed <= TEST_TIME);

	debug("BINSTR COUNT: %lu, END TIME: %d, OPS: %f",
		find_count, (int)elapsed, (double)find_count / elapsed);
	
	return NULL;
}

char *test_find_performance()
{
	int i = 0;
	int found_at = 0;
	unsigned long find_count = 0;
	time_t elapsed = 0;
	time_t start = time(NULL);

	do {
		for(i = 0; i < PERFORMANCE_ITER; i++)
		{
			found_at = String_find(&IN_STR, get_random_bstr());
			find_count++;
		}

		elapsed = time(NULL) - start;
	} while(elapsed <= TEST_TIME);

	debug("FIND COUNT: %lu, END TIME: %d, OPS: %f",
		find_count, (int)elapsed, (double)find_count / elapsed);

	found_at = found_at; //to remove warning of unused

	return NULL;
}

char *test_scan_performance()
{
	int i = 0;
	int found_at = 0;
	unsigned long find_count = 0;
	time_t elapsed = 0;
	StringScanner *scan = StringScanner_create(&IN_STR);

	time_t start = time(NULL);

	do {
		for(i = 0; i < PERFORMANCE_ITER; i++)
		{
			found_at = 0;
			do {
				found_at = StringScanner_scan(scan, get_random_bstr());
				find_count++;

			} while(found_at != -1);
		}

		elapsed = time(NULL) - start;
	} while (elapsed <= TEST_TIME);

	debug("SCAN COUNT: %lu, END TIME: %d, OPS: %f",
		find_count, (int)elapsed, (double)find_count / elapsed);

	StringScanner_destroy(scan);

	return NULL;
}

char *all_tests()
{
	mu_suite_start();

	mu_run_test(setup_bstrlist);
	mu_run_test(test_find_and_scan);

#if 0
	mu_run_test(test_binstr_performance);
	mu_run_test(test_find_performance);
	mu_run_test(test_scan_performance);
#endif

	mu_run_test(destroy_global_bstrings);
	return NULL;
}

RUN_TESTS(all_tests);
