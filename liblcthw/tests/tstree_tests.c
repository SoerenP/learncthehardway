#include <lcthw/minunit.h>
#include <lcthw/tstree.h>
#include <string.h>
#include <assert.h>
#include <lcthw/bstrlib.h>

TSTree *node = NULL;
char *valueA = "VALUEA";
char *valueB = "VALUEB";
char *value2 = "VALUE2";
char *value4 = "VALUE4";
char *reverse = "VALUER";
int traverse_count = 0;

static char *test1 = "TEST";
static char *test2 = "TEST2";
static char *test3 = "TSET";
static char *test4 = "T";

int test1_len = 4;
int test2_len = 5;
int test3_len = 4;
int test4_len = 1;


char *test_insert()
{
	node = TSTree_insert(node, test1, test1_len, valueA);
	mu_assert(node != NULL, "Failed to insert into tst.");

	node = TSTree_insert(node, test2, test2_len, value2);
	mu_assert(node != NULL, "Failed to insert into tst with second name.");

	node = TSTree_insert(node, test3, test3_len, reverse);
	mu_assert(node != NULL, "Failed to insert into tst with reverse name.");

	node = TSTree_insert(node, test4, test4_len, value4);
	mu_assert(node != NULL, "Failed to insert into tst with second name.");

	return NULL;
}

char *test_search_exact()
{
	// tst returns the last one inserted
	void *res = TSTree_search(node, test1, test1_len);
	mu_assert(res == valueA, "Got the wrong value back, should get A not B.");

	// tst does not find if not exact
	res = TSTree_search(node, "TESTNO", strlen("TESTNO"));
	mu_assert(res == NULL, "Should not find anything.");

	return NULL;
}

char *test_search_prefix()
{
	void *res = TSTree_search_prefix(node, test1, test1_len);
	debug("result: %p, expected: %p", res, valueA);
	mu_assert(res == valueA, "Got wrong valueA by prefix.");

	res = TSTree_search_prefix(node, "TEST", 1);
	debug("result: %p, expected: %p", res, valueA);
	mu_assert(res == value4, "Got wrong value4 for prefix of 1.");

	res = TSTree_search_prefix(node, "TE", strlen("TE"));
	mu_assert(res != NULL, "Should find for short prefix.");

	res = TSTree_search_prefix(node, "TE--", strlen("TE--"));
	mu_assert(res != NULL, "Should find for partial prefix.");

	return NULL;
}

void TSTree_traverse_test_cb(void *value, void *data)
{
	assert(value != NULL && "Should not get NULL value.");
	assert(data == valueA && "Expecting valueA as the data.");
	traverse_count++;
}

char *test_traverse()
{
	traverse_count = 0;
	TSTree_traverse(node, TSTree_traverse_test_cb, valueA);
	debug("traverse count is: %d", traverse_count);
	mu_assert(traverse_count == 4, "Didn't find 4 keys.");

	return NULL;
}

char *test_destroy()
{
	TSTree_destroy(node);

	return NULL;
}

char *all_tests()
{
	mu_suite_start();

	mu_run_test(test_insert);
	mu_run_test(test_search_exact);
	mu_run_test(test_search_prefix);
	mu_run_test(test_traverse);
	mu_run_test(test_destroy);

	return NULL;
}

RUN_TESTS(all_tests);
