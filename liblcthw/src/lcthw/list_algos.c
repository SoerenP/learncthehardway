#include <lcthw/list_algos.h>
#include <lcthw/dbg.h>
/* Sorting linked lists is a really bad idea. You have no random access, and fidling with pointers when sorting gives */
/* an insane amount of cache misses - it will thus be limitied to stuff like merge and bubblesort, and will not be able */
/* to take advantage of things like case associativity in an intuitive way */
inline void ListNode_swap(ListNode *a, ListNode *b)
{
	check(a != NULL && b != NULL, "Recieved NULL pointer");
	void *temp = a->value;
	a->value = b->value;
	b->value = temp;
error:
	return;
}

int List_bubble_sort(List *list, List_compare cmp)
{
	check(list != NULL, "Recieved NULL Pointer");
	int count = List_count(list);	

	if(count <= 1) return 0; //Allready sorted

	int newn = 0;
	do {	
		newn = 0;	
		LIST_LOOP_ITERATOR(list, first, next, cur, count){
			if(cur->next){ //Does it have a next?
				if(cmp(cur->value,cur->next->value) > 0) //is List[i] less than List[i-1]? 
				{
					ListNode_swap(cur,cur->next); //then swap their values (dont swap nodes, too many pointers)
					//sorted = 0; //Take another turn around the while loop
					newn = _i;
				}
			}
		}
		count = newn;
	} while(!(count == 0));


	return 0;
error:
	return 1; //something fucked up
}

//Subroutine for the merge part of the merge_sort method. 
inline List *List_merge(List *left, List *right, List_compare cmp)
{
	List *new = List_create();
	check_mem(new);

	void *val = NULL;

	while(List_count(left) > 0 || List_count(right) > 0)
	{
		if(List_count(left) > 0 && List_count(right) > 0){
			if(cmp(List_first(left),List_first(right)) <= 0){
				val = List_shift(left);
			} else {
				val = List_shift(right);
			}
			List_push(new,val);
		} else if(List_count(left) > 0) {
			val = List_shift(left);
			List_push(new,val);
		} else if(List_count(right) > 0) {
			val = List_shift(right);
			List_push(new,val);
		}
	}

error: //Even if new is null, we just return it, so same statement.
	return new;
}

List *List_merge_sort(List *list, List_compare cmp)
{
	check(list != NULL, "Recieved NULL pointer");

	//Basecase: One element, the list is sorted
	if(List_count(list) <= 1)
		return list;

	//Recursive case. First, *divide* the list into equal-sized sublists.
	List *left, *right;

	left = List_create();
	check_mem(left);

	right = List_create();
	check_mem(right);

	int middle = List_count(list) / 2;

	LIST_FOREACH(list, first, next, cur){
		if(middle > 0){//still putting stuff into left. 
			List_push(left,cur->value);
		} else {
			List_push(right,cur->value);
		}
		middle--;
	}

	//Recursively sort both sublists
	List *sorted_left = List_merge_sort(left, cmp);
	check_mem(sorted_left);
	List *sorted_right = List_merge_sort(right, cmp);
	check_mem(sorted_right);

	if(sorted_left != left) List_destroy(left); //Hvis de er ens lister skal de ikke ødelægges! pga basecase returnerer samme pointer
	if(sorted_right != right) List_destroy(right); //hvis de er ens lister skal de ikke ødelægges! pga basecater returnerer samme pointer

	//Then merge the now-sorted sublists
	List *merged = List_merge(sorted_left, sorted_right, cmp);
	List_destroy(sorted_left);
	List_destroy(sorted_right);

	return merged;

error:
	return NULL;
}
