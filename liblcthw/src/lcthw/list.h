#ifndef lcthw_List_h
#define lcthw_List_h

#include <stdlib.h>

/* Double Linked list: Deletion is easier that single linked list (since we now predecessor by definition) */
/* SIngle Linked list: Can be the tail of multiple heads - not possible for double linked list (think tree like structure) */


struct ListNode;
/* ListNode are the Nodes in the linked list. Has pointers in either direction, and a void ptr, as to allow the node to contain "anything" */
typedef struct ListNode{
	struct ListNode *next;
	struct ListNode *prev;
	void *value;
} ListNode;

/* A list has a count for elements, and refferences to both the first and last to make certain operations constant time */
typedef struct List {
	int count;
	ListNode *first;
	ListNode *last;
} List;

/* Constructors and destructors */
List *List_create(); //Returns a list struct (empty)
void List_destroy(List *list); //Attempts to free the NODES! (not values) O(n)
void List_clear(List *list); //Attempt to free the VALUES! (not nodes) O(n)
void List_clear_destroy(List *list); //Attempts to both destroy and clear O(n)
List *List_copy(List *list); //Makes a SHALLOW copy of the input list (it sets value of new nodes to those of the old!)
List *List_join(List *left, List *right); //todo

/* Macros for accessing members of a List struct */
#define List_count(A) ((A)->count)
#define List_first(A) ((A)->first != NULL ? (A)->first->value : NULL)
#define List_last(A) ((A)->last != NULL ? (A)->last->value : NULL)

/* Functions for manipulating and retrieving the nodes of the lists */
void List_push(List *list, void *value); //Adds element to the end of the list O(1). 
void *List_pop(List *list); //Retrieves the first element (the end!), and deletes it from the list, O(1)

void List_unshift(List *list, void *value); //Adds element to the front of the list, O(1)
void *List_shift(List *list); //Removes first element and returns it, O(1)

void *List_remove(List *list, ListNode *node); //Removes the node from the list. 

/* Macro for easier looping through a set of nodes. */
/* L is a pointer to a List */
/* S is the start point of the loop */
/* M is the pointer to update V to after an iteration */
/* V is a pointer to contain the referencess we loop over */
/* example: LIST_FOREACH(list, first, next, cur) loops through the entire list */
/* and keeps the current node in cur, increments to next each loop, and starts at firsts */
#define LIST_FOREACH(L, S, M, V) ListNode *_node = NULL; \
	ListNode *V = NULL; \
	for(V = _node = L->S; _node != NULL; V = _node = _node->M)

/* Marcro for loop control as above, but with an iterator value going up for more control */
#define LIST_LOOP_ITERATOR(L, S, M, V, N) ListNode *_node = NULL; \
	ListNode *V = NULL; \
	int _i = 0; \
	for(V = _node = L->S;((_i < N) && (_node != NULL)); V = _node = _node->M, _i++)

#endif
