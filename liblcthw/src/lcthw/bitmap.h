#ifndef _bitmap_h
#define _bitmap_h

#define Bit_2_int_ratio 32

struct Bitmap {
	int *B; //the bitvector
	int bits;
	int B_size;
};

struct Bitmap *Bitmap_create(int bits);
struct Bitmap *Bitmap_create_random(int bits);
void Bitmap_free(struct Bitmap *bm);
int fromBitmapToInt32(struct Bitmap *bm, int start_bit, int end_bit);
int fromBitmapToInt(struct Bitmap *bm, int start_bit, int end_bit);
int fromBitmapToInt32Const(struct Bitmap *bm, int start_bit, int end_bit);

void Bitmap_set(struct Bitmap *b, int k);
void Bitmap_set_l(struct Bitmap *b, long k);
void Bitmap_clear(struct Bitmap *b, int k);
int Bitmap_test(struct Bitmap *b, int k);
void Bitmap_print(struct Bitmap *b);
int Bitmap_naive_rank(struct Bitmap *b, int m);
int Bitmap_naive_rank_subrange(struct Bitmap *b, int start_bit, int end_bit);
int Bitmap_naive_select(struct Bitmap *b, int m);

#endif
