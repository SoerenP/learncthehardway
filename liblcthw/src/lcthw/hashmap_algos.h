#ifndef hashmap_algos_h
#define hashmap_algos_h

#include <stdint.h>

/* All algorithms only take a void pointer. They should take a length */
/* But, they all interpret the data in chuncks of bytes (8 bit) */
/* As such, they convert the data to a bstring and check the length */
/* Not super pretty, but done as to being able to hash not just numbers, */
/* But strings / "anything" */

uint32_t Hashmap_fnv1a_hash(void *data);

uint32_t Hashmap_adler32_hash(void *data);

uint32_t Hashmap_djb_hash(void *data);

uint32_t Hashmap_division_hash(void *data);

uint32_t Hashmap_bj_hash(void *data);

uint32_t Hashmap_ELF_hash(void *data);

#endif
