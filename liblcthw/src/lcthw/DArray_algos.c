#include <lcthw/DArray_algos.h>
#include <stdlib.h>

/* A wrapper to get c's stdlib implementation to sort our stuff for us */
int DArray_qsort(DArray *array, DArray_compare cmp)
{
	qsort(array->contents, DArray_count(array), sizeof(void *), cmp);
	return 0;
}
