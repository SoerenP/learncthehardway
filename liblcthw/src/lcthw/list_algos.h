#ifndef _lcthw_List_algos_h
#define _lcthw_List_algos_h

#include <lcthw/list.h>

typedef int (*List_compare)(const void *a, const void *b); //You can fx say (List_compare)strcmp :)

//returns 0 on success, 1 on error. Cmp can be (casted) strcmp
int List_bubble_sort(List *list, List_compare cmp);

/* Returns a pointer to the input list if it was already sorted */
/* Else it returns a new sorted list */
/* Merge sort doesnt require random access, so it is ideal for linked lists */
List *List_merge_sort(List *list, List_compare cmp);


#endif
