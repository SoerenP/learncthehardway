#include <stdio.h>
#include <stdlib.h>
#include "limits.h"
#include "bitmap.h"
#include "compact.h"
#include <math.h>
#include "dbg.h"

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))


/* given amount of bits wanted, creates array of int to encompass these */
/* one int is 4 byte = 32 bits */
/* bit k is at the arrayindex A[k/32] */
/* and in that 4 bytes = 32 bits, its position will be k % 32 */
/* the compact module implements these mappings, so this is just a WRAPPER */
struct Bitmap *Bitmap_create(int bits)
{
	struct Bitmap *new = malloc(sizeof(struct Bitmap));
	check_mem(new);
	
	int i,size;	
	size = (bits/Bit_2_int_ratio)+1;
	if(size < 1) size = 1;

	new->B = malloc(sizeof(int)*size);
	check_mem(new->B);	

	//Make DAMN sure that the bitmap is all 0 bits. I dont trust calloc. This way we are sure 
	for(i=0; i < size; i++)
		new->B[i] = 0;
	
	new->bits = bits;
	new->B_size = size;

	return new;
error:
	return NULL;
}

struct Bitmap *Bitmap_create_random(int bits)
{
	struct Bitmap *new = malloc(sizeof(struct Bitmap));
	check_mem(new);

	int i, size;
	size = (bits/Bit_2_int_ratio)+1;
	if(size < 1) size = 1;
	
	new->B = malloc(sizeof(int)*size);
	check_mem(new->B);	

	//set entries to random - the bits in the bitmap will by extension be random
	for(i=0; i < size; i++)
	{
		new->B[i] = rand(); 
	}

	new->bits = bits;
	new->B_size = size;
		
	return new;	
error:
	return NULL;

}



int fromBitmapToInt32(struct Bitmap *bm, int start_bit, int end_bit)
{
	//check for legal input
	check((start_bit >= 0) && (end_bit >= start_bit) && (bm->bits >= end_bit),\
	"Incorrehent start_bit and end_bit values");
	check(bm != NULL, "Recieved NULL pointer");
	
	unsigned int i, sum, end, bitsize;
	sum = 0;
	bitsize = 1;
	end = MIN(end_bit-start_bit,Bit_2_int_ratio-1);
	for(i = 0; i <= end; i++)
	{
		sum += Bitmap_test(bm,i+start_bit) * bitsize;
		bitsize=bitsize<<1;
	}
	return sum;
error:
	return -1;
}

int fromBitmapToInt32Const(struct Bitmap *bm, int start_bit, int end_bit)
{

	//check for legal input
	check((start_bit >= 0) && (end_bit >= start_bit) && (bm->bits >= end_bit),\
	"Incorrehent start_bit and end_bit values");
	check(bm != NULL, "Recieved NULL pointer");

  int start_block = start_bit/32;
  int end_block = end_bit/32;
  int start_offset = start_bit%32;
  int end_offset = end_bit%32;

  if (start_block == end_block)
    {
    	unsigned one = 1;
    	unsigned mask = (one<<(end_offset+1))- 1 - (one<<(start_offset)) + 1;
      return (bm->B[start_block] & mask)>>start_offset;
    } 
  else 
    {
      unsigned zero = 0;
      unsigned start_mask = zero-1 - ((1<<(start_offset))-1);
      unsigned end_mask = ((1<<(end_offset+1))-1);
      unsigned most_significant_bits = (bm->B[end_block] & end_mask)<<(32-start_offset);
      unsigned least_significant_bits = (bm->B[start_block] & start_mask)>>start_offset;
      
      return most_significant_bits + least_significant_bits;
    }

error: //Fallthrough
  return -1;
}

/* Exponentiation by squarring */
static int ipow(int base, int exp)
{
	int result = 1;
	while (exp)
	{
		if(exp & 1) //if exponent has a bit set, we need do multiply with base
			result *= base;
		exp >>= 1; //basecase is that we double
		base *= base;
	}

	return result;
}

int fromBitmapToInt(struct Bitmap *bm, int start_bit, int end_bit)
{
	check((start_bit >= 0) && (end_bit >= start_bit) && (bm->bits >= end_bit),\
	"Incorrehent start_bit and end_bit values");
	check(bm != NULL, "Recieved NULL pointer");


	int i, sum;
	sum = 0;
	for(i = 0; i <= end_bit-start_bit; i++)
	{
		if(Bitmap_test(bm,start_bit+i))
		{
			sum += ipow(2,i);
		}
	}
	return sum;
error:
	return -1;
}


void Bitmap_free(struct Bitmap *bm)
{
	if(bm && bm->B)
	{
		free(bm->B);
		free(bm);
	}
}


void Bitmap_set(struct Bitmap *b, int k)
{
	set_bit(b->B,k);
}

void Bitmap_set_l(struct Bitmap *b, long k)
{
	set_bit_l(b->B,k);
}

void Bitmap_clear(struct Bitmap *b, int k)
{
	clear_bit(b->B,k);
}

int Bitmap_test(struct Bitmap *b, int k)
{
	return test_bit(b->B,k);
}

void Bitmap_print(struct Bitmap *b)
{
	print_bits(b->B,b->bits);
}

int Bitmap_naive_rank(struct Bitmap *b, int m)
{
	return naive_rank(b->B,b->bits, m);
}

int Bitmap_naive_rank_subrange(struct Bitmap *b, int start_bit, int end_bit)
{
	return naive_rank_subrange(b->B,b->bits,start_bit,end_bit);
}

int Bitmap_naive_select(struct Bitmap *b, int m)
{
	return naive_select(b->B,b->bits,m);
}
