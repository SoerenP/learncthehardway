#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include "compact.h"

	/*	En integer er mest efficient at access i hukommelse! 
	*	Vi mapper fra plads i integer til plads i bit-vectoren. 
	*	Element med index k i bit aray er:
	*	Bit position k % 32 i det array element med index k/32. 
	*	Could just make functions as macros actually
	*/

int M = 100;

void set_bit(int *A, int k)
{
	// flag 1 skiftes til højre k positioner (inden i en int, dvs 4 byte = 32!)
	// herefter XOR vi den korrekte position jævnfør ovenover, i den korrekte INTEGER i arrayet!	
	A[k/32] |= (1 << (k%32));
}

void set_bit_l(int *A, long k)
{
  // flag 1 skiftes til højre k positioner (inden i en int, dvs 4 byte = 32!)
	// herefter XOR vi den korrekte position jævnfør ovenover, i den korrekte INTEGER i arrayet!	
	A[k/32] |= (1 << (k%32));
}

void clear_bit(int *A, int k)
{
	// AND med en maske der sætter alt som det var før, undtagen position k, som er 0. 
	// flag = 111111...k=0...11111
	A[k/32] &= ~(1 << (k%32)); 
}

int test_bit(int *A, int k)
{
	// lav en passende mask med 1 i pos k. shift 1 k gange
	// and masken med passende integer. Hvis bitwise & mellem mask og integer er 1, er bitten sat!
	return ((A[k/32] & (1 << (k%32)))!=0); 
}

void print_bits(int *A, int size)
{
	printf("\n");
	int i;
	for(i = 0; i < size; i++)
	{
		if(test_bit(A,i)) printf("1");
		else printf("0");
	}
	printf("\n");
}

void test_bits(int size, int *to_set, int setsize, int *to_clear, int clearsize)
{
	int A[size]; //32 x size bits
	int i;
	for(i = 0; i < size; i++)
		A[i] = 0;

	for(i = 0; i < setsize; i++)
		set_bit(A,to_set[i]);
	
	print_bits(A,size);
	printf("Naive rank(%d): %d\n",size,naive_rank(A,size,size));
	printf("Naive select(%d): %d\n",setsize,naive_select(A,size,setsize));

	for(i = 0; i < clearsize; i++)
		clear_bit(A,to_clear[i]);
	
	print_bits(A,size);
}

int naive_rank(int *A, int size, int m)
{
	// Sequential scan, O(n) query time, constant n space used.
	if(m > size){
		return naive_rank(A,size,size);
	}

	int i,res;
	res = 0;
	for(i=0; i <= m; i++)
		if(test_bit(A,i)) res++;

	return res;
}

int naive_rank_subrange(int *A, int size, int start_bit, int end_bit)
{
	if(end_bit > size){
		return naive_rank_subrange(A,size,start_bit,size);
	}
	int i,res;
	res = 0;
	for(i=start_bit; i <= end_bit; i++)
		if(test_bit(A,i)) res++;
		
	return res;	
}

int naive_select(int *A, int size, int m)
{
	// Sequential scan, O(n) query time, constant n space used. 
	if(m > size){
		printf("ooops naive select");
		return naive_select(A,size,size);
	}

	int i,res;
	res = 0;
	i = 0;
	while(res < m && i < size)
	{
		if(test_bit(A,i)) res++;
		i++;
	}

	return i;
}
