#include <lcthw/string_algos.h>
#include <limits.h>

/* i suspect this is the bottlekneck, since we dont use all 256 characters */
/* instead it should only make an array for the characters we actually have in the string */
static inline void String_setup_skip_chars(
	size_t *skip_chars,
	const unsigned char *needle, ssize_t nlen)
{
	size_t i = 0;
	size_t last = nlen - 1;

	for(i = 0; i < UCHAR_MAX + 1; i++)
	{
		skip_chars[i] = nlen;
	}

	for(i = 0; i < last; i++){
		skip_chars[needle[i]] = last - i;
	}
}

static inline const unsigned char *String_base_search(
	const unsigned char *haystack, ssize_t hlen,
	const unsigned char *needle, ssize_t nlen,
	size_t *skip_chars)
{
	size_t i = 0;
	size_t last = nlen - 1;

	assert(haystack != NULL && "Given bad haystack to search.");
	assert(needle != NULL && "Given bad needle to search for.");

	check(nlen > 0, "nlen can't be <= 0");
	check(hlen > 0, "hlen can't be <= 0");

	while(hlen >= nlen)
	{
		for(i = last; haystack[i] == needle[i]; i--){
			if(0 == i){
				return haystack;
			}
		}
	
		hlen -= skip_chars[haystack[last]];
		haystack += skip_chars[haystack[last]];
	}

error: //fallthrough
	return NULL;
}

/* does the BMH algorithm in one shot */
/* i.e we do the setup, then we do the search with the two above methods */
int String_find(bstring in, bstring what)
{
	const unsigned char *found = NULL;

	const unsigned char *haystack = (const unsigned char *)bdata(in);
	ssize_t hlen = blength(in);
	const unsigned char *needle = (const unsigned char *)bdata(what);
	ssize_t nlen = blength(what);
	size_t skip_chars[UCHAR_MAX + 1] = {0};

	String_setup_skip_chars(skip_chars, needle, nlen);

	found = String_base_search(haystack, hlen, needle, nlen, skip_chars);

	return found != NULL ? found - haystack : -1;
}


StringScanner *StringScanner_create(bstring in)
{
	StringScanner *scan = calloc(1, sizeof(StringScanner));
	check_mem(scan);

	scan->in = in;
	scan->haystack = (const unsigned char *)bdata(in);
	scan->hlen = blength(in);

	assert(scan != NULL && "fuck");
	return scan;

error:
	if(scan) free(scan);
	return NULL;
}

static inline void StringScanner_set_needle(StringScanner *scan, bstring tofind)
{
	scan->needle = (const unsigned char *)bdata(tofind);
	scan->nlen = blength(tofind);

	String_setup_skip_chars(scan->skip_chars, scan->needle, scan->nlen);
}

static inline void StringScanner_reset(StringScanner *scan)
{
	scan->haystack = (const unsigned char*)bdata(scan->in);
	scan->hlen = blength(scan->in);
}


/* Uses a string scanner struct to look in, instead of the array that find uses */
/* this means that we can build the struct once, and then repedeatly run the scan in the tests */
/* as to only do the setup once, and then do the scanning using only the second method (no setup) repeadetly*/
/* the idea is then to see what impact the setup has (i.e find) versus just the scanning (scan) */
int StringScanner_scan(StringScanner *scan, bstring tofind)
{
	const unsigned char *found = NULL;
	ssize_t found_at = 0;

	if(scan->hlen <= 0){
		StringScanner_reset(scan);
		return -1;
	}

	if((const unsigned char *)bdata(tofind) != scan->needle){
		StringScanner_set_needle(scan, tofind);
	}

	found = String_base_search(
		scan->haystack, scan->hlen,
		scan->needle, scan->nlen,
		scan->skip_chars);

	if(found) {
		found_at = found - (const unsigned char *)bdata(scan->in);
		scan->haystack = found + scan->nlen;
		scan->hlen -= found_at - scan->nlen;
	} else {
		//done, reset the setup
		StringScanner_reset(scan);
		found_at = -1;
	}

	return found_at;
}

void StringScanner_destroy(StringScanner *scan)
{
	if(scan) free(scan);
}
