#ifndef _radixmap_h
#include <stdint.h>

/* the data struct is really a 64 bit integer, but we treat it as 2 */
/* 32 bit integers so we can have a key, and a value */
typedef union RMElement {
	uint64_t raw;
	struct {
		uint32_t key;
		uint32_t value;
	} data;
} RMElement;

typedef struct RadixMap {
	size_t max;
	size_t end;
	uint32_t counter;
	RMElement *contents;
	RMElement *temp;
} RadixMap;

/* sorting is done in bytes. We first sort by the first byte (1's) */
/* then we do it by the 10's and so on */
/* in the end we will have them correctly sorted */

RadixMap *RadixMap_create(size_t max);

void RadixMap_destroy(RadixMap *map);

void RadixMap_sort(RadixMap *map);

RMElement *RadixMap_find(RadixMap *map, uint32_t key);

int RadixMap_add(RadixMap *map, uint32_t key, uint32_t value);

int RadixMap_delete(RadixMap *map, RMElement *el);

#endif
