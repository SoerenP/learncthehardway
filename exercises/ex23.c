#include <stdio.h>
#include <string.h>
#include "dbg.h"
#include <time.h>
#include <string.h>

int normal_copy(char *from, char *to, int count)
{
	int i = 0;

	for(i = 0; i < count; i++)
	{
		to[i] = from[i];
	}
	return i;
}

int duffs_device(char *from, char *to, int count)
{
	{
		int n = (count + 7) / 8;
		switch(count % 8){
			case 0: do {*to++ = *from++;
				case 7: *to++ = *from++;
				case 6: *to++ = *from++;
				case 5: *to++ = *from++;
				case 4: *to++ = *from++;
				case 3: *to++ = *from++;
				case 2: *to++ = *from++;
				case 1: *to++ = *from++;

			} while(--n > 0);
		}
	}
	return count;
}

int zeds_device(char *from, char *to, int count)
{
	{
		int n = (count + 7) / 8;

		switch(count % 8){
			case 0:
			again: *to++ = *from++;

			case 7: *to++ = *from++;
			case 6: *to++ = *from++;
			case 5: *to++ = *from++;
			case 4: *to++ = *from++;
			case 3: *to++ = *from++;
			case 2: *to++ = *from++;
			case 1: *to++ = *from++;
				if(n-- > 0) goto again;
		}
	}
	return count;
}

int valid_copy(char *data, int count, char expects)
{
	int i = 0;
	for(i = 0; i < count; i++)
	{
		if(data[i] != expects){
			log_err("[%d] %c != %c", i, data[i], expects);
			return 0;
		}
	}
	return 1;
}

int main(int argc, char **argv)
{
	clock_t begin, end;
	double time_spent;

	int size = 1000;

	char from[1000] = {'a'};
	char to[1000] = {'c'};
	int rc = 0;

	// setup the from to have some stuff
	memset(from, 'x',size);
	//set it to a failure mode
	memset(to, 'y', size);
	check(valid_copy(to,size,'y'), "Not initialized right");

	// use normal copy to
	begin = clock();
	rc = normal_copy(from, to, size);
	end = clock();
	time_spent = (double)(end-begin)/CLOCKS_PER_SEC;
	log_info("normal copy finished with\t %f",time_spent);

	check(rc == 1000, "Normal copy failed: %d", rc);
	check(valid_copy(to,size,'x'),"Normal copy failed");

	// reset
	memset(to, 'y', size);

	// duffs version
	begin = clock();
	rc = duffs_device(from,to,size);
	end = clock();
	time_spent = (double)(end-begin)/CLOCKS_PER_SEC;
	log_info("duff copy finished with\t %f",time_spent);
	check(rc == size, "Duff's device failed:%d",rc);
	check(valid_copy(to,size,'x'), "Duff's device failed copy");

	// reset
	memset(to, 'y', size);

	begin = clock();
	memcpy(to,from,size);
	end = clock();
	time_spent = (double)(end-begin)/CLOCKS_PER_SEC;
	log_info("mem copy finished with \t %f",time_spent);
	//zeds version
	//rc = zeds_device(from, to, 1000);
	//check(rc == 1000, "Zed's device failed: %d", rc);
	//check(valid_copy(to, 1000, 'x'), "Zed's device failed to copy.");


	memset(to, 'y', size);

	begin = clock();
	memmove(to,from,size);
	end = clock();
	time_spent = (double)(end-begin)/CLOCKS_PER_SEC;
	log_info("mem copy finished with \t %f",time_spent);



	return 0;
error:
	return 1;
}

