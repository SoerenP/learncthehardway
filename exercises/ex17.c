#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#define MAX_DATA 512
#define MAX_ROWS 100

/* Did not convert to singleton, since i know that pattern inside out, and i dont really like it
	its easy, but it gets messy (manager for managers etc :/)
 */ 


/* By default */
int max_rows = MAX_ROWS;
int max_data = MAX_DATA;

enum Skill{
	Noob,
	Scrub,
	Decent,
	Tryhard,
	Pro
};

const char* getSkillName(enum Skill skill)
{
	switch(skill)
	{
		case Noob:
			return "Noob";
		case Scrub:
			return "Scrub";
		case Decent:
			return "Decent";
		case Tryhard:
			return "Tryhard";
		case Pro:
			return "Pro";
		default:
			return "Unspec";
	}
}

struct Address {
	int id;
	int set;
	char name[MAX_DATA];
	char email[MAX_DATA];
	enum Skill skill;
};

struct Database {
	int d_max_rows;
	int d_max_data;
	struct Address rows[MAX_ROWS];
};

struct Connection {
	FILE *file;
	struct Database *db;
};

void die(const char *message, struct Connection *conn)
{
	if(errno) {
		perror(message);
	} else {
		printf("ERROR: %s\n",message);
	}
	
	Database_close(conn);
	exit(1);
}


void Address_print(struct Address *addr)
{
	printf("%d %s %s %s\n",
		addr->id, addr->name, addr->email, getSkillName(addr->skill));
}

void Database_load(struct Connection *conn)
{
	int rc = fread(conn->db, sizeof(struct Database), 1, conn->file);
	if(rc != 1) die("Failed to load Database.",conn);
}

struct Connection *Database_open(const char *filename, char mode, int max_data, int max_rows)
{
	struct Connection *conn = malloc(sizeof(struct Connection));
	if(!conn) die("Memory error",conn);	
	
	conn->db = malloc(sizeof(struct Database));
	if(!conn->db) die("Memory error",conn);

	if(mode == 'c') {
		conn->file = fopen(filename, "w");
		conn->db->d_max_data = max_data;
		conn->db->d_max_rows = max_rows;
	} else {
		conn->file = fopen(filename, "r+");

		if(conn->file){
			Database_load(conn);
		}
	}
	
	if(!conn->file) die("Failed to open the file",conn);

	return conn;
}

void Database_close(struct Connection *conn){
	if(conn){
		if(conn->file) fclose(conn->file);
		if(conn->db) free(conn->db);
		free(conn);
	}
}

void Database_write(struct Connection *conn)
{
	rewind(conn->file);

	int rc = fwrite(conn->db, sizeof(struct Database), 1, conn->file);

	if(rc != 1) die("Failed to write to database.",conn);

	rc = fflush(conn->file);
	if(rc == -1) die("Cannot flush database.",conn);

}

void Database_create(struct Connection *conn, int size)
{
	int i = 0;
	
	for(i = 0; i < size; i++)
	{
		/* make a prototype to initialize it */
		struct Address addr = {.id = i, .set = 0};
		/* then just assign it */
		conn->db->rows[i] = addr;
	}
}

void Database_set(struct Connection *conn, int id, const char *name, const char *email, int skill)
{
	struct Address *addr = &conn->db->rows[id];
	if(addr->set) die("Already set, delete it first",conn);
	if(sizeof(name) > conn->db->d_max_data) die("name greater than max_data",conn);
	if(sizeof(email) > conn->db->d_max_data) die("email greater than max_data",conn);	


	addr->set = 1;
	char *res = strncpy(addr->name, name, conn->db->d_max_data);
	/* strncpy does not append null of input is greater than MAX_DATA, only if its less, so manually set it */
	if(conn->db->d_max_data > 0) addr->name[conn->db->d_max_data-1] = '\0';
	if(!res) die("Name copy failed",conn);

	res = strncpy(addr->email,email,conn->db->d_max_data);
	if(conn->db->d_max_data > 0) addr->email[conn->db->d_max_data-1] = '\0';		
	if(!res) die("Email copy failed",conn);

	addr->skill = skill;
}

void Database_get(struct Connection *conn, int id)
{
	struct Address *addr = &conn->db->rows[id];

	if(addr->set){
		Address_print(addr);
	} else {
		die("ID is not set",conn);
	}
}

void Database_delete(struct Connection *conn, int id)
{
	struct Address addr = {.id = id, .set = 0};
	conn->db->rows[id] = addr;
}

void Database_list(struct Connection *conn)
{
	int i = 0;
	struct Database *db = conn->db;
	
	for(i = 0; i < conn->db->d_max_rows; i++){
		struct Address *cur = &db->rows[i];

		if(cur->set) {
			Address_print(cur);
		}
	}
}


void Database_sort_by_skill(struct Connection *conn, int skill)
{
	int i;
	for(i = 0; i < conn->db->d_max_rows; i++)
	{
		struct Address *cur = &conn->db->rows[i];
		if(cur->set && (cur->skill == skill)) Address_print(cur);
	}
}

void Database_truncate(struct Connection *conn)
{
	int lower, upper, stop = 0;
	int i,j;
	for(i = 0; i < conn->db->d_max_rows; i++)
	{
		if(stop == 1) break;
		struct Address *cur = &conn->db->rows[i];

		if(!(cur->set))
		{
			lower = cur->id;
			for(j = lower; j < conn->db->d_max_rows; j++)
			{
				struct Address *up_cur = &conn->db->rows[j];
				if(up_cur->set)
				{
					upper = up_cur->id;
					Address_CopyFromTo_Index(conn,upper,lower);
					break;
				}
				if(j == conn->db->d_max_rows-1) stop=1; 
			}
		}
	}
}


void Address_CopyFromTo_Index(struct Connection *conn, int upper, int lower)
{
	struct Address *from = &conn->db->rows[upper];
	
	Database_set(conn, lower, from->name, from->email, from->skill);
	Database_delete(conn,upper);
}

void Database_find(struct Connection *conn, char *target)
{
	int i = 0;
	int success = 0;
	
	struct Database *db = conn->db;

	for(i = 0; i < conn->db->d_max_rows; i++){
		struct Address *cur = &db->rows[i];
		if(cur->set)
		{
			if(strcmp(cur->name,target) == 0)
			{
				Address_print(cur);
				success++;		
			}
			if(strcmp(cur->email,target) == 0)
			{
				Address_print(cur);
				success++;			
			}
		}
	}
	if(success < 1) printf("Did not find %s\n",target);

}

int main(int argc, char *argv[])
{

	/* Die needs a pointer, so here we go.. */ 
	struct Connection *conn = NULL;

	if(argc < 3) die("USAGE: ex17 <dbfile> <action> [action params]",conn);
	
	char *filename = argv[1];
	char action = argv[2][0];

	/* if creating, take sizes */
	if(argc > 4 && (action == 'c')){
		int a = atoi(argv[3]);
		int b = atoi(argv[4]);
		max_rows = a;
		max_data = b;
		if(a > MAX_ROWS) die("Maximal row size exceeded\n",conn);
		if(b > MAX_DATA) die("Maximal data size exceeded\n",conn);
		if(b % 4 != 0) die("Data size must be divisible by 4\n",conn);
	}

	conn = Database_open(filename, action,max_data,max_rows);

	int id = 0;	
	char *target;
	uint skill = 0;
	enum Skill test = Pro;	

	/* dette bør rydes op. Lav function der tjekker for hver commando og ikke tjekker for de andre? */
	if(argc > 3 && ((action != 'c') && (action != 'l') && (action != 't') && (action != 'z'))) 
	{
		id = atoi(argv[3]);
		skill = atoi(argv[6]);			
		if(skill > test) die("Skill simply too high",conn);
	}
	if(id >= conn->db->d_max_rows && action != 'f') die("There's not that many records.",conn);
	if(argc > 3 && action == 'f') target = argv[3];

	if(argc > 3 && action == 'z') skill = atoi(argv[3]);
	if(skill > test) die("Skill simply too high",conn);

	switch(action) {
		case 'c':
			if( argc < 5) die("Need rowsize and datasize to create",conn);
			Database_create(conn,conn->db->d_max_rows);
			Database_write(conn);
			break;

		case 'g':
			if( argc != 4) die("Need an id to get",conn);
				
			Database_get(conn, id);
			break;

		case 's':
			if(argc != 7) die("Need id, name, email, skill to set",conn);

			Database_set(conn, id, argv[4], argv[5], skill);
			Database_write(conn);
			break;

		case 'd':
			if(argc != 4) die("Need id to delete",conn);

			Database_delete(conn, id);
			Database_write(conn);
			break;

		case 'l':
			Database_list(conn);
			break;

		case 'f':
			if(argc != 4) die("Need argument to find",conn);
			Database_find(conn,target);
			break;

		case 't':
			Database_truncate(conn);
			Database_write(conn);
			break;

		case 'z':
			if(argc != 4) die("Need skill to search for",conn);
			Database_sort_by_skill(conn,skill);
			break;

		default:
			die("Invalid action, only: c=create, g=get, s=set, d=del, l=list, f=find, t=truncate",conn);
		}

	Database_close(conn);

	return 0;
}
