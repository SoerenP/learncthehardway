#include "shell.h"
#include "dbg.h"
#include <stdarg.h>
//Givet en template struct, finder den de elementer i args af template som matcher det der er i VAR ARGS og erstater med argumentet
//Efter denne. Fx Shell_exec(CURL_SH,"URL",url,"TARGET",depends_file,NULL); tager CURL_SH template og sætter url ind i arg listen istf"URL" osv.
int Shell_exec(Shell template, ...)
{
	apr_pool_t *p = NULL; //Lav pointer til tom pool
	int rc = -1; //Return value
	apr_status_t rv = APR_SUCCESS; //Antag succes fra start af
	va_list argp; //Variable argument liste
	const char *key = NULL; //Key til at lede efter argumenter i shell struct
	const char *arg = NULL; //Til at execute argumentet
	int i = 0;
	int args_replaced = 0;

	rv = apr_pool_create(&p, NULL); //bed om en pool af hukommelse 
	check(rv == APR_SUCCESS, "Failed to create pool"); //kunne vi lave en pool?

	va_start(argp, template); //Start listen af variable arguments

	for(key = va_arg(argp, const char *); //sæt key til første argument som er et argument 
		key != NULL; // Så længe der stadig er argumenter
		key = va_arg(argp, const char *)) // Opdater til næste argument 
	{
		arg = va_arg(argp, const char *); //Få argumentet som pointer til str

		for(i = 0; template.args[i] != NULL; i++) //Gå igennem hver argument i shell
		{
			if(strcmp(template.args[i], key) == 0) //Hvis det argument vi er kaldt med er defineret i den shell vi fik
			{
				template.args[i] = arg; // Så sætter vi den 
				args_replaced++; //Noter at vi fandt argumentet og erstatede det
				break; //found it
			}
		}
	}

	check(args_replaced == template.args_count, "Failed to replace all arguments");//Check om alle argumenter fundet

	rc = Shell_run(p, &template); //Når vi har ledt alle argumenter igennem og fundet de rigtige kører vi lortet
	apr_pool_destroy(p); //ødelæg hele memory pool
	va_end(argp); //stop med at lede efter argumenter
	return rc; //hvis run gik godt, returner vi det samme

error:
	if(p){
		apr_pool_destroy(p);
	}
	return rc;
}

int Shell_run(apr_pool_t *p, Shell *cmd)
{
	//vi får givet en memory pool og en shell med commandoer

	apr_procattr_t *attr; //Lav en process attr struct
	apr_status_t rv; //Lav en til status
	apr_proc_t newproc; //Lav en til processen

	rv = apr_procattr_create(&attr, p); //forsøg at lave attribut struct
	check(rv == APR_SUCCESS, "Failed to create proc attr.");

	rv = apr_procattr_io_set(attr, APR_NO_PIPE, APR_NO_PIPE, APR_NO_PIPE); //Set I_O til processen
	check(rv == APR_SUCCESS, "Failed to set IO of command.");

	rv = apr_procattr_dir_set(attr, cmd->dir); //Set hvilken mappe den skal køre i
	check(rv == APR_SUCCESS, "Failed to set root to %s.", cmd->dir);

	rv = apr_procattr_cmdtype_set(attr, APR_PROGRAM_PATH); //sæt kommando typen
	check(rv == APR_SUCCESS, "Failed to set cmd type.");

	rv = apr_proc_create(&newproc, cmd->exe, cmd->args, NULL, attr, p); //Lav processen 
	check(rv == APR_SUCCESS, "Failed to run command.");

	rv = apr_proc_wait(&newproc, &cmd->exit_code, &cmd->exit_why, APR_WAIT); //Sæt den til at vente
	check(rv == APR_CHILD_DONE, "Failed to wait.");

	check(cmd->exit_code == 0, "%s exited badly.", cmd->exe);
	check(cmd->exit_why == APR_PROC_EXIT, "%s was killed or crashed", cmd->exe);

	return 0;

error:
	return -1;
}

// Ryder op i tmp folder efter installation
Shell CLEANUP_SH = {
	.exe = "rm",
	.dir = "/tmp",
	.args = {"rm", "-rf", "/tmp/pkg-build", "/tmp/pkg-src.tar.gz","/tmp/pkg-src.tar.bz2", "/tmp/DEPENDS", NULL}
};

//kalder git på en URL
Shell GIT_SH = {
	.dir = "/tmp",
	.args_count = 1,
	.exe = "git",
	.args = {"git", "clone", "URL", "pkg-build", NULL}
};

//kalder TAR på en url
Shell TAR_SH = {
	.dir = "/tmp",
	.args_count = 1,
	.exe  = "git",
	.args = {"tar", "-xzf", "FILE", "--strip-components","1", NULL}
};

Shell CURL_SH = {
	.dir = "/tmp",
	.args_count = 2,
	.exe = "curl",
	.args = {"curl", "-L", "-o", "TARGET", "URL", NULL}
};

//Kører config fra det hentede
Shell CONFIGURE_SH = {
	.exe = "./configure",
	.args_count = 1,
	.dir = "/tmp/pkg-build",
	.args = {"configure", "OPTS", NULL}
};

//Kører make fra det hentede
Shell MAKE_SH = {
	.exe = "make",
	.args_count = 1,
	.dir = "/tmp/pkg-build",
	.args = {"make", "OPTS", NULL}
};

//kører install
Shell INSTALL_SH = {
	.exe = "sudo",
	.args_count = 1,
	.dir = "/tmp/pkg-build",
	.args = {"sudo", "make", "TARGET", NULL}
};
