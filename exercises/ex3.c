#include <stdio.h>

int main()
{
	/* This is the max int in c :D */
	int age = 2147483647;
	int height = 72;

	printf("I am %d years old.\n",age);
	printf("I am %d inches tall.\n",height);

	return 0;
}
