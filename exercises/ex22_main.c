#include "ex22.h"
#include "dbg.h"
#include <stdlib.h>

const char *MY_NAME = "Søren E. Pettersson";
const int const_int = 100;
//static means that the int below can only be seen in this file!? 
static int static_int = 10;

struct TryToGetMe {
	int *top;
	int *bottom;
	const int secret;
};


void pointer_demo()
{
	struct TryToGetMe *t = malloc(sizeof(struct TryToGetMe));
	int top = 1;
	int bottom = 2;

	t->top = &top;
	t->bottom = &bottom;

	//Writing to a constant value by casting away the constant!? wtf C?
	*(int *)&t->secret = 4;

	log_info("top address %d",*(t->top));
	log_info("bottom address %d",*(t->bottom));

	log_info("access bottom from struct pointer arithmetic: %d",*(t+(sizeof(int *))));

	log_info("const int secret: %d",t->secret);

	*(int *)&t->secret = 5;

	log_info("const int secret: %d",t->secret);
	free(t);
}


void scope_demo(int count)
{
	log_info("count is %d", count);

	if(count > 10){
		int count = 100; //BAD! BUGS!

		log_info("count in this scope %d", count);
	}

	log_info("count is at exit: %d",count);

	count = 3000;

	log_info("count after assign: %d",count);
}

int main(int argc, char **argv)
{
	// test out THE_AGE accessors
	log_info("My name: %s, age: %d", MY_NAME, get_age());

	set_age(100);

	log_info("My age is now: %d", get_age());

	//test out THE_SIzE extern
	log_info("THE_SIZE is: %d", THE_SIZE);
	print_size();

	THE_SIZE = 9;

	log_info("ThE_SIZE is now: %d", THE_SIZE);
	print_size();

	// test the ratio function static
	log_info("Ratio at first: %f", update_ratio(2.0));
	log_info("Ratio again: %f", update_ratio(10.0));
	log_info("Ratio once more: %f", update_ratio(300.0));

	// test the scope demo
	int count = 4;
	scope_demo(count);
	scope_demo(count * 20);

	log_info("count ater caling scope_demo: %d", count);


	//test static and const changing?
	log_info("const_int before: %d",const_int);

	log_info("static_int before: %d", static_int);
	static_int = 2;
	log_info("static_int after: %d", static_int);


	//test pass by reference vs. pass by value
	int value = 99;
	int *ref = &value;
	log_info("Value before: %d",value);

	pass_by_value(value);
	pass_by_reference(ref);
	log_info("Value after: %d",*ref);

	pointer_demo();

	return 0;
}
