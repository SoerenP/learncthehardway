#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

/*	Function pointer:
 *	Write it: char *make_coolness(int awesome_sauce)
 *	Wrapt it: char (*make_coolness)(int awesome_sauce)
 *  Rename it: char *(*coolness_cb)(int awesome_sauce) -> en pointer til en function som tager en int, ret en char.
 *  Pointeren hedder da coolness_cb
 */




/** our old friend die from ex17 */

/* fail, and let OS clean up the mess */
void die(const char *message)
{
	if(errno) {
		perror(message);
	} else {
		printf("ERROR: %s\n",message);
	}

	exit(1);
}

/* a typedef creates a fake type, in this */
/* case for a function pointer */
typedef int (*compare_cb)(int a, int b);
typedef int *(*sorting_alg)(int *numbers, int count, compare_cb cmp); 
/**
 * A classic bubble sort function that uses the
 * compare_cb to do the sorting
 */

int *bubble_sort(int *numbers, int count, compare_cb cmp)
{
	int temp = 0;
	int i = 0;
	int j = 0;
	int *target = malloc(count * sizeof(int));

	if(!target) die("Memory error.");

	memcpy(target,numbers,count * sizeof(int));

	for(i = 0; i < count; i++)
	{
		for(j = 0; j < count - 1; j++)
		{
			if(cmp(target[j],target[j+1]) > 0)
			{
				temp = target[j+1];
				target[j+1] = target[j];
				target[j] = temp;
			}
		}
	}

	return target;
}

int sorted_order(int a, int b)
{
	return a-b;
}

int reverse_order(int a, int b){
	return b-a;
}

int strange_order(int a, int b)
{
	if(a == 0 || b == 0)
	{
		return 0;
	} else {
		return a % b;
	}
}

/**
  * Quicksort!
  */
int *insertion_sort(int *numbers, int count, compare_cb cmp)
{
	int i,j,temp;
	int *target = malloc((sizeof(int)*count));

	if(!target) die("Memory error.");

	memcpy(target,numbers,count * sizeof(int));	

	for(i=1; i < count; i++)
	{
		j = i;
		while ((j > 0) && (target[j-1] > target[j])){
		temp = target[j];
		target[j] = target[j-1];
		target[j-1] = temp;
		j--; 
		}
	}
	return target;
}

/**
 * Used to test that we are sorting things correctly
 * by doing sorting and printing it out
 *
 */


void test_sorting(int *numbers, int count, compare_cb cmp, sorting_alg sort)
{
	int i = 0;
	int *sorted = sort(numbers, count, cmp);
	
	if(!sorted) die("Failed to sort a sequence.");

	for(i = 0; i < count; i++)
	{
		printf("%d ",sorted[i]);
	}
	printf("\n");
	free(sorted);

	unsigned char *data = (unsigned char *)cmp;
	
	for(i = 0; i < 25; i++){
		printf("%02x:",data[i]);
	}
	printf("\n");
}

int main(int argc, char *argv[])
{
	if(argc < 2) die("USAGE: ex18 4 3 1 5 6");

	int count = argc - 1;
	int i = 0;
	char **inputs = argv + 1; /* brilliant. */

	int *numbers = malloc(count * sizeof(int));
	if(!numbers) die("Memory error.");

	for(i = 0; i < count; i++)
	{
		numbers[i] = atoi(inputs[i]);
	}

	printf("Bubble sort\n");
	test_sorting(numbers,count,sorted_order,bubble_sort);
	test_sorting(numbers,count,reverse_order,bubble_sort);
	test_sorting(numbers,count,strange_order,bubble_sort);

	printf("Insertion sort\n");
	test_sorting(numbers,count,sorted_order,insertion_sort);

	free(numbers);
	
	return 0;
}
