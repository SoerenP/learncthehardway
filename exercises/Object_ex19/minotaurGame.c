#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "cmdLineRPG.h"
/* #define NDEBUG */ /* uncomment to remove assert */
#include <assert.h>

Object MapProto = {
	.init = Map_init,
	.move = Map_move,
	.attack = Map_attack
};

Object RoomProto = {
	.move = Room_move,
	.attack = Room_attack
};

Object MonsterProto = {
	.init = Monster_init,
	.attack = Monster_attack
};

int Map_init(void *self)
{
	Map *map = self;

	/* make some rooms for a small map */
	Room *hall = NEW(Room, "The great hall");
	Room *throne = NEW(Room, "The throne room");
	Room *arena = NEW(Room, "The arena with the minotaur");
	Room *kitchen = NEW(Room, "Kitchen, you have the knife now");
	Room *secretroom = NEW(Room, "The secret room behind the throne, with the servant");



	/* put the bad guy in the arena */
	arena->bad_guy = NEW(Monster, "The evil minotaur");
	secretroom->bad_guy = NEW(Monster, "The hiding servant");

	/* Setup the map rooms */
	hall->north = throne;
	
	throne->north = secretroom;
	throne->west = arena;
	throne->east = kitchen;
	throne->south = hall;

	arena->east = throne;
	kitchen->west = throne;
	secretroom->south = throne;

	map->start = hall;
	map->location = hall;

	return 1;
}

int main(int argc, char *argv[])
{
	/* simle way to set up randomness */
	srand(time(NULL));

	/* make our map to work with */
	Map *game = NEW(Map, "The Hall of the Minotaur.");
	printf("COMMANDS: a:attack, l:directions, n:NORTH, e:EAST, w:WEST, s:SOUTH.\n");
	printf("You enter the ");
	game->location->_(describe)(game->location);

	while(process_input(game)){
	}

	return 0;
}
