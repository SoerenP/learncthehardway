#ifndef _object_h
#define _object_h
#include <stddef.h>

typedef enum {
	NORTH, SOUTH, EAST, WEST
} Direction;

/* et object er mere en prototype. Det har felter til function pointers. Vi kan sætte dem vi vel
 * og lade dem være vi ikke skal bruge, så længe vi tjekker før vi bruger dem. Så vi kan bygge 
 * objekter ud fra hvad de skal have af understående. Hvis de ikke får assignet noget "specifikt"
 * "nedarver" de bare fra objektet. Dvvs et objekt har nogle standard funktioner, da det laes får det assignet
 * de standard funktioner der er vha. pointers. Når vi laver noget nyt, kan vi implementer nye funktioner,
 * der skal overholde de samme funktion pointers, og sætte dem til noget andet.
 * Fx, monster overskriver attack på en måde (skad monsteret), hvor et rum bare printer at du er dum.
 */

typedef struct {
	char *description;
	int (*init)(void *self);
	void (*describe)(void *self);
	void (*destroy)(void *self);
	void *(*move)(void *self, Direction direction);
	int (*attack)(void *self, int damage);
} Object;

/* prototypes */
int Object_init(void *self);
void Object_destroy(void *self);
void Object_describe(void *self);
void *Object_move(void *self, Direction direction);
int Object_attack(void *self, int damage);
void *Object_new(size_t size, Object proto, char *description);

#define NEW(T, N) Object_new(sizeof(T), T##Proto, N)
#define _(N) proto.N

#endif
