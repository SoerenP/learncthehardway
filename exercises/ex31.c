#include <unistd.h>

//to debug using gdb:
//ps ax | grep ex31 to find process number
//sudo gdb ./ex31 #process number
//to set breakpoint, break ex31.c:line so it finds the right file
//use p i to see value of i
//use list to see entire program
//set var i = 200 to set var to 200
//continue and it will exit normally

int main(int argc, char **argv)
{
	int i = 0;

	while(i < 100)
	{
		usleep(3000);
	}

	return 0;
}
