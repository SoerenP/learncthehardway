#include <stdio.h>
#define STACKSIZE 512

struct Stack {
	int S[STACKSIZE];
	int S_top;
};

struct Stack *create_Stack()
{
	struct Stack *stack = malloc(sizeof(struct Stack)+1);
	stack->S_top = 0;
	return stack;
}

void free_Stack(struct Stack *stack)
{
	free(stack);
}

int Stack_empty(struct Stack *stack)
{
	if(stack->S_top == 0)
		return 1;
	else return 0;
}

void Stack_Push(struct Stack *stack, int x)
{
	stack->S_top += 1;
	stack->S[stack->S_top] = x;
}

int Stack_Pop(struct Stack *stack)
{
	if(Stack_empty(stack))
		return -1;
	else stack->S_top -= 1;
		return stack->S[stack->S_top+1];
}

int main(){
	struct Stack *stack = create_Stack();
	int i;
	for(i=0; i < 100; i++)
	{
		Stack_Push(stack,i);
	}
	for(i=0; i < 100; i++)
	{
		printf("Stack pop %d\n",Stack_Pop(stack));
	}
	free(stack);


	return 0;
}
