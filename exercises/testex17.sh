set -e
#!/bin/bash
max_rows=10
max_size=12

echo "Removing old Database"
if [ -f test.dat ];
then
	rm test.dat
fi

echo "Creating new database"
ex17 test.dat c $max_rows $max_size

echo "Setting each other person"
ex17 test.dat s 0 test1 test1 0

ex17 test.dat s 2 test2 test2 1

ex17 test.dat s 4 test3 test3 2

ex17 test.dat s 6 test4 test4 3

ex17 test.dat s 8 test5 test5 4

ex17 test.dat s 9 deleteMe Please 0
ex17 test.dat l
echo "Deleting Person"

ex17 test.dat d 9
ex17 test.dat l

echo "Truncating"

ex17 test.dat t
ex17 test.dat l

echo "Find Person"

for person in test1 test2 test3 test4 test5
do
	ex17 test.dat f $person
done

echo "Find skillz"
for skillz in 0 1 2 3 4
do
	ex17 test.dat z $skillz
done

