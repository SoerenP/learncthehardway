#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>

struct Person {
	char *name;
	int age;
	int height;
	int weight;
};

struct Person Person_create_stack(char *name, int age, int height, int weight)
{
	struct Person who;
	who.name = strdup(name);
	who.age = age;
	who.height = height;
	who.weight = weight;

	return who;
}

struct Person Person_print_stack(struct Person who)
{
	printf("Name: %s\n",who.name); //access the field of the struct via its pointer, -> is members! 
	printf("\tAge: %d\n",who.age); //same
	printf("\tHeight: %d\n",who.height); //same
	printf("\tWeight: %d\n",who.weight); //same

}

struct Person Person_destroy_stack(struct Person who)
{
	free(who.name);
}


//Returnerer en pointer til et person struct!
struct Person *Person_create(char *name, int age, int height, int weight)
{
	// Ask OS for memory, with the size of a Person and its fields
	struct Person *who = malloc(sizeof(struct Person));
	//use the nice library to assert the expression. if malloc fail, we will thus abort, since then who pointer is null	
	assert(who != NULL);

	who->name = strdup(name); //make sure that the person has the actual string, and not a pointer to the string 
	who->age = age; //age is just a copy, so make our int point to it
	who->height = height;//same
	who->weight = weight; //same

	return who; //return the POINTER to the struct who (noite the function is def to return Person *Per... a POINTER
}

void Person_destroy(struct Person *who)
{
	assert(who != NULL); //assert that we dont have a null pointer (can you free null`?!)

	free(who->name); //remember the string! strdup is like malloc
	free(who); //free the entire struct, but after, since if you did it first, you could not go to the filed with the string!
}

void Person_print(struct Person *who)
{
	printf("Name: %s\n",who->name); //access the field of the struct via its pointer, -> is members! 
	printf("\tAge: %d\n",who->age); //same
	printf("\tHeight: %d\n",who->height); //same
	printf("\tWeight: %d\n",who->weight); //same
}

int main(int argc, char *argv[])
{
	// make two people structures
	struct Person *sren = Person_create("Sren pettersson",23,75,179);

	struct Person *sanne = Person_create("Sanne Jespersen",24,60,175);

	// print them out and where they are in memory
	printf("Sren is at memory location %p\n",sren);
	Person_print(sren);

	printf("Sanne is at memory location %p\n",sanne);
	Person_print(sanne);

	// make everyone age 20 years and print them again
	sren->age += 20;
	sren->height -= 2;
	sren->weight += 40;
	Person_print(sren);

	sanne->age += 20,
	sanne->height -= 2;
	Person_print(sanne);

	Person_destroy(sren);
	Person_destroy(sanne);

	struct Person bob = Person_create_stack("Bob sagget",100,100,100);
	printf("Bob is at memory location %p\n",bob);
	Person_print_stack(bob);
	
	bob.age += 100;
	bob.weight += -2;

	Person_print_stack(bob);

	Person_destroy_stack(bob);





	return 0;
}
