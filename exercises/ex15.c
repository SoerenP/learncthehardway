#include <stdio.h>

void print_with_array_access(int *ages, char *names[])
{

	int i = 0;

	while(names[i] != NULL && ages[i] != NULL)
	{
		printf("%s is years %d old.\n",
			names[i], ages[i]);
		i++;
	}
	return 0;
}

void print_with_pointer_arith(int *ages, char *names[])
{
	int i = 0;
	while(*(names+i) != NULL && *(ages+i) != NULL)
	{
		printf("%s has lived %d years.\n",
			*(names+i),*(ages+i));
		i++;
	}
}

void print_with_hacks(int *ages, char *names[],int count)
{
	int *cur_age = ages;
	int **cur_name = names;	

	while((cur_age - ages) < count)
	{
		printf("%s lived %d years so far.\n",
			*cur_name, *cur_age);
		cur_age++;
		cur_name++;
	}
}

int main(int argc, char *argv[])
{
	// create two arrays we care about
	int ages[] = {23, 43, 12, 89, 2};

	char *names[] = {
		"Alan", "Frank", "Mary", "John", "Lisa"
	};

	// safely get the size of ages
	int count = sizeof(ages)/sizeof(int);
	int i = 0;
	
	printf("---\n");

	// print out pointers!
	print_with_array_access(ages,names);	
	printf("---\n");	
	print_with_pointer_arith(ages,names);
	printf("---\n");
	print_with_hacks(ages,names,count);
	return 0;
}
