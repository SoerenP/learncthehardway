#include <stdio.h>
#include "dbg.h"
#include <dlfcn.h>

typedef int(*lib_function)(int length, const char *data);

#define MAX_SIZE 128

int check_size_message(const char *msg)
{
	int i = 0;
	while(msg[i] != '\0' && i < MAX_SIZE)
		i++;
	return i;
}


int main(int argc, char **argv)
{
	int rc = 0;
	check(argc == 4, "USAGE: ex29 libex29.so function data");

	char *lib_file = argv[1];
	char *func_to_run = argv[2];
	char *data = argv[3];

	int size = check_size_message(data);

	void *lib = dlopen(lib_file, RTLD_NOW);
	check(lib != NULL, "Failed to open library %s: %s", lib_file, dlerror());

	lib_function func = dlsym(lib, func_to_run);
	check(func != NULL, "Did not find %s function in the library %s: %s", func_to_run, lib_file, dlerror());

	rc = func(size,data);
	check(rc == 0, "Function %s return %d for data: %s", func_to_run, rc, data);

	rc = dlclose(lib);
	check(rc == 0, "Failed to close %s", lib_file);

	return 0;

error:
	return 1;
}
