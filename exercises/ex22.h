#ifndef _ex22_h
#define _ex22_h

// make THE_SIZE in ex22.c available to other .c files
extern int THE_SIZE;
//gets and sets an intertal static variable in ex2..c
int get_age();
void set_age(int age);

//updates a static variable that's inside update_ratio
double update_ratio(double ratio);

void print_size();

void pass_by_value(int value);
void pass_by_reference(int *ref);

#endif
