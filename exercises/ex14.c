#include <stdio.h>
#include <ctype.h>
#include <string.h>

/*forward declarations */
void print_letters(char arg[]);

void print_arguments(int argc, char *argv[])
{
	int i = 0;
	char **p_argv = argv; /* pointer til en pointer til char*/

	for(i = 0; i < argc; i++){
		print_letters(*(p_argv+i)); //tag pointeren, følg den, lig i til, for at hoppe til i'te element i array af chars
	}
}

void print_letters(char arg[])
{
	int i = 0;
	char *p_char = arg; //pointer thl char

	for(i = 0; arg[i] != '\0'; i++){
		char ch = *(p_char+i);

		if(isalpha(ch) || isblank(ch)) {
			printf("'%c' == %d ", ch, ch);
		}
	}
	printf("\n");
}

int main(int argc, char *argv[])
{
	char **p_argv = argv;
	print_arguments(argc, p_argv);
	return 0;
}
