#include <stdio.h>
#include "ex22.h"
#include "dbg.h"

int THE_SIZE = 1000;
static int THE_AGE = 37;
int get_age()
{
	return THE_AGE;
}

void set_age(int age)
{
	THE_AGE = age;
}

double update_ratio(double new_ratio)
{
	static double ratio = 1.0;

	double old_ratio = ratio;
	ratio = new_ratio;

	return old_ratio;
}

void print_size()
{
	log_info("I think size is: %d", THE_SIZE);
}

void pass_by_value(int value)
{
	log_info("I got me some value: %d", value);
}

void pass_by_reference(int *ref)
{
	log_info("Inside function i had: %d",*ref);
	static int temp = 10;
	ref = &temp;
	log_info("What about before exit: %d",*ref);
}
